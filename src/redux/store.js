/**
 * 
 * Создание redux-store
 * 
 * 
 */

import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";

import authReducer from './reducers/auth-reducer';
import appReducer from "./reducers/app-reducer";
import objectsReducer from "./reducers/objects-reducer";
import countersReducer from "./reducers/counters-reducer";

// Редьюсеры
const reducers = combineReducers({
    auth: authReducer,
    app: appReducer,
    objects: objectsReducer,
    counters: countersReducer,
});

const composeEnchancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// Создание Store
const store = createStore(reducers, composeEnchancers(applyMiddleware(thunk)));

export default store;
