/**
 *  Редьюсер приложения
 *
 *
 */
import {appAPI} from "../../api/api";
import toXmlRequest from "../../utils/toXmlRequest";
import xml2object from "../../utils/xml2object";

// Названия действий

const
    LOAD_GEO_TYPES = 'objects/LOAD_GEO_TYPES',
    LOAD_OBJECTS = 'objects/LOAD_OBJECTS';


// Инициализация

const initialState = {
    objectsGeoTypes: null,
    objects: null,
    status: {
        isGeoTypesLoaded: false,
        isObjectsLoaded: false,
    }
};


// Reducer

const objectsReducer = (state = initialState, action) => {

    switch (action.type) {

        case LOAD_GEO_TYPES:
            return {
                ...state,
                objectsGeoTypes: action.objectsGeoTypes,
                status: {
                    ...state.status,
                    isGeoTypesLoaded: action.status,
                },
            };

        case LOAD_OBJECTS:
            return {
                ...state,
                objects: action.objects,
                status: {
                    ...state.status,
                    isObjectsLoaded: action.status,
                },
            };

        default: return state;
    }
};

export default objectsReducer;


// Actions

export const getGeoTypesAction = (objectsGeoTypes, status) => ({type: LOAD_GEO_TYPES, objectsGeoTypes, status});
export const getObjectsAction = (objects, status) => ({type: LOAD_OBJECTS, objects, status});


// Thunks

/**
 * Получить гео-объекты на карте
 *
 * @returns {Function}
 */
export const getGeoTypes = () => async $d => {
    try {

        const {data} = await appAPI.getData(toXmlRequest({operation: 'GEO_Get_Node_Types'}));

        if (data) {
            const
                {xml_doc: {response_data: {row: rows}}} = xml2object(data);

            $d(getGeoTypesAction(rows, true));

            return rows;
        } else {
            throw new Error('Гео-данные не были загружены');
        }
    } catch (error) {
        console.group('===[ Ошибка загрузки объектов ]===');
        console.error(error);
        console.groupEnd();

        return null;
    }
};


/**
 * Получить объекты по геотипу
 *
 * @param {Object} geoTypesObject - гео-типы
 * @returns {Function}
 */
export const getObjects = (geoTypesObject=null) => async $d => {
    try {
        const
            {data} = await appAPI.getData(toXmlRequest({operation: 'GEO_Get_Object_Mass'}));

        let
            geoTypes = geoTypesObject;

        if (!geoTypesObject) {
            const
                {data: dataGeoTypes} = await appAPI.getData(toXmlRequest({operation: 'GEO_Get_Node_Types'})),
                {xml_doc: {response_data: {row: geoTypesLoaded}, status: statusGeoTypes}} = xml2object(dataGeoTypes);

            if (parseInt(statusGeoTypes) < 300) {
                geoTypes = geoTypesLoaded;
            } else {
                throw new Error(`Ошибка загрузки гео-типов при загрузке объектов. Статус запроса: ${statusGeoTypes}`);
            }
        }

        if (data && geoTypes) {
            const
                {xml_doc: {response_data: {row: rows}}} = xml2object(data);

            const objects = geoTypes.reduce((acc, type) => ({
                ...acc,
                [type.ID_Object_Node_Type]: {
                    name: type.Object_Node_Type,
                    objects: rows && rows.filter(object => parseInt(object.ID_Object_Node_Type) === parseInt(type.ID_Object_Node_Type))
                }
            }), {});

            $d(getObjectsAction(objects, true));

            return true;
        } else {
            throw new Error('Объекты или гео-типы полностью отсутствуют.');
        }
    } catch (error) {
        console.group('===[ Ошибка загрузки объектов ]===');
        console.error(error);
        console.groupEnd();

        return false;
    }
};


/**
 * Удаляет все данные по объектам
 *
 * @returns {Function}
 */
export const clearDataObjects = () => $d => {
    $d(getGeoTypesAction(null, false));
    $d(getObjectsAction(null, false));
};
