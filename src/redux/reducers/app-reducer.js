/**
 *  Редьюсер приложения
 *
 *
 */
import {appAPI} from "@src/api/api";
import toXmlRequest from "@src/utils/toXmlRequest";
import xml2object from "@src/utils/xml2object";
import updateStatusDarkMode from "@src/utils/updateStatusDarkMode";
import decimalToHexString from "@src/utils/toHex";
import {getAuth} from "./auth-reducer";
import {clearDataObjects, getGeoTypes, getObjects} from "./objects-reducer";
import uploadInfoByObjectID from "../../utils/uploadInfoByObjectID";

// Названия действий

const
    INITIAL_APP = 'app/INITIAL_APP',
    CHANGE_CHOSEN_MARKER = 'app/CHANGE_CHOSEN_MARKER',
    CHANGE_TYPE_MAP = 'app/CHANGE_TYPE_MAP',
    CHANGE_INFO_ITEM_ON_MAP = 'app/CHANGE_INFO_ITEM_ON_MAP',
    SET_LOADED_INFO_OBJECT_STATUS = 'app/SET_LOADED_INFO_OBJECT_STATUS',
    SET_STATUS_MAP = 'app/SET_STATUS_MAP',
    SET_CENTER_MAP = 'app/SET_CENTER_MAP',
    OPEN_LEFT_PANEL = 'app/OPEN_LEFT_PANEL',
    UPDATE_FILTER = 'app/UPDATE_FILTER',
    OPEN_ITEM_ON_MAP_STATUS = 'app/OPEN_ITEM_ON_MAP_STATUS',
    SET_OPEN_SETTINGS = 'app/SET_OPEN_SETTINGS',
    SET_DARK_STYLE = 'app/SET_DARK_STYLE';


// Инициализация

const initialState = {
    statusMap: null, // Статусы карты
    chosenMarker: null, // Выбранный маркер на карте
    mapInfoItem: null, // Подробная информация об объекте
    settings: {
        zoom: 16, // Стартовый зум карты
        positionMap: [45.058997, 38.95518], // Начальная позиция карты
    },
    filterList: null,
    status: {
        initialApp: false,
        isLeftPanelOpen: false,
        isSettingsOpen: false,
        isMapInfoItemOpen: false,
        isDarkStyle: localStorage.getItem('mp-dark-status') === 'true',
        // typeMap: localStorage.getItem('mp-type-map'),
        typeMap: 'osm',
        fetch: {
            isLoadedStatus: false,
            isLoadedInfoByObject: false, // Загрузка информации по объекту
        },
    },
};


// Reducer

const appReducer = (state = initialState, action) => {

    switch (action.type) {

        case INITIAL_APP:
            return {
                ...state,
                status: {
                    ...state.status,
                    initialApp: action.status,
                },
            };

        case SET_CENTER_MAP:
            return {
                ...state,
                settings: {
                    ...state.settings,
                    positionMap: action.positionMap,
                }
            };

        case CHANGE_CHOSEN_MARKER:
            return {
                ...state,
                chosenMarker: action.chosenMarker,
            };

        case CHANGE_TYPE_MAP:
            return {
                ...state,
                status: {
                    ...state.status,
                    typeMap: action.typeMap,
                }
            };

        case CHANGE_INFO_ITEM_ON_MAP:
            return {
                ...state,
                mapInfoItem: action.infoByItem
            };

        case UPDATE_FILTER:
            return {
                ...state,
                filterList: action.filterName,
            };

        case SET_STATUS_MAP:
            return {
                ...state,
                statusMap: action.statusMap,
                status: {
                    ...state.status,
                    fetch: {
                        ...state.status.fetch,
                        isLoadedStatus: action.status,
                    }
                },
            };

        case OPEN_LEFT_PANEL:
            return {
                ...state,
                status: {
                    ...state.status,
                    isLeftPanelOpen: action.status,
                }
            };

        case SET_DARK_STYLE:
            return {
                ...state,
                status: {
                    ...state.status,
                    isDarkStyle: action.status,
                }
            };

        case SET_OPEN_SETTINGS:
            return {
                ...state,
                status: {
                    ...state.status,
                    isSettingsOpen: action.status,
                }
            };

        case OPEN_ITEM_ON_MAP_STATUS:
            return {
                ...state,
                status: {
                    ...state.status,
                    isMapInfoItemOpen: action.status,
                }
            };

        case SET_LOADED_INFO_OBJECT_STATUS:
            return {
                ...state,
                status: {
                    ...state.status,
                    fetch: {
                        ...state.status.fetch,
                        isLoadedInfoByObject: action.status,
                    }
                }
            };

        default: return state;
    }
};

export default appReducer;


// Actions

export const setInitialAppAction = (status) => ({type: INITIAL_APP, status});
export const changeChosenMarkerAction = (chosenMarker) => ({type: CHANGE_CHOSEN_MARKER, chosenMarker});
export const changeTypeMapAction = typeMap => ({type: CHANGE_TYPE_MAP, typeMap});
export const changeInfoItemOnMapAction = infoByItem => ({type: CHANGE_INFO_ITEM_ON_MAP, infoByItem});
export const updateFilterListAction = filterName => ({type: UPDATE_FILTER, filterName});
export const setStatusMapAction = (statusMap, isFetched) => ({type: SET_STATUS_MAP, statusMap, status: isFetched});
export const openLeftPanelAction = (status) => ({type: OPEN_LEFT_PANEL, status});
export const changeDarkStyleAction = (status) => ({type: SET_DARK_STYLE, status});
export const openSettingsAction = (status) => ({type: SET_OPEN_SETTINGS, status});
export const openMapInfoItemAction = (status) => ({type: OPEN_ITEM_ON_MAP_STATUS, status});
export const setStatusLoadedInfoObjectAction = (status) => ({type: SET_LOADED_INFO_OBJECT_STATUS, status});
export const setPositionCenterMapAction = (positionMap) => ({type: SET_CENTER_MAP, positionMap});


// Thunks

/**
 * Инициализация приложения
 *
 * @returns {Function}
 */
export const setInitialApp = () => async $d => {
    try {
        const
            isAuth = await $d(getAuth());

        if (isAuth) {
            const
                geoTypes = await $d(getGeoTypes()),
                isLoadedObjects = await $d(getObjects(geoTypes)),
                typeMap = localStorage.getItem('mp-type-map'),
                isLoadedStatus = await $d(getStatus(typeMap));

            if (!isLoadedObjects || !isLoadedStatus) {
                throw new Error('Данные не загрузились.')
            }
        }
    } catch (error) {
        console.group('=== Ошибка инициализации приложения ===');
        console.error(error);
        console.groupEnd();

        return false;
    }

    $d(setInitialAppAction(true));
};


/**
 * Подгрузка статусов
 *
 * @param typeMap
 * @returns {Function}
 */
export const getStatus = (typeMap) => async $d => {
    updateStatusDarkMode(localStorage.getItem('mp-dark-status') === 'true', typeMap);

    try {
        const {data} = await appAPI.getData(toXmlRequest({operation: 'GEO_Get_Statistics'}));

        if (data) {
            const
                {xml_doc: {response_data: {row: rows}, status}} = xml2object(data);

            if (parseInt(status) < 300) {
                await $d(setStatusMapAction(rows.reduce((obj, item) => ({
                    ...obj,
                    [item['ID']]: {
                        ...item,
                        'BG_Color': item['BG_Color']
                            ? '#'+decimalToHexString(parseFloat(item['BG_Color']))
                            : parseInt(item['ID']) === 0
                                ? '#179A28'
                                : '#ffffff'
                    }
                }), {})), true);

                return true;
            } else {
                throw new Error(`Статус ответа сервера: ${status}`);
            }
        } else {
            throw new Error('Объект с данными - пустой.');
        }
    } catch (error) {
        console.group('===[ Ошибка загрузки статусов ]===');
        console.error(error);
        console.groupEnd();

        return false;
    }
};


/**
 * Меняет стиль приложения
 *
 * @param status
 * @param typeMap
 * @returns {Function}
 */
export const changeDarkStyle = (status, typeMap='yandex') => $d => {
    localStorage.setItem('mp-dark-status', status);
    updateStatusDarkMode(status, typeMap);
    $d(changeDarkStyleAction(status));
};


/**
 * Изменить тип карты
 *
 * @param type
 * @returns {Function}
 */
export const setTypeMap = (type) => async $d => {
    localStorage.setItem('mp-type-map', type);
    $d(changeTypeMapAction(type));
};


/**
 * Сбрасывает все данные приложения
 *
 * @returns {Function}
 */
export const clearAppData = () => $d => {
    $d(openLeftPanelAction(false));
    $d(setStatusMapAction(null, false));
    $d(clearDataObjects());
};


/**
 * Открывает\закрывает полную информацию об объекте
 * на странице карты
 *
 * @param {Number | String | Null} objectId - ID объекта в системе
 * @param {Boolean} status - открыть(true) \ закрыть(false)
 * @param {Object} objectInfo - Информация об объекте
 *
 * @returns {Function}
 */
export const openMapInfoItem = (status=false, objectId=null, objectInfo={}) => async $d => {
    $d(openMapInfoItemAction(status));

    if (status && objectId) {
        try {
            $d(setStatusLoadedInfoObjectAction(true));

            const
                data = await uploadInfoByObjectID(objectId);

            data.objectInfo = {...objectInfo, id: objectId};

            if (data) {
                $d(changeInfoItemOnMapAction(data));
            }
        } catch (error) {
            console.group('===[ Ошибка подгрузки информации ]===');
            console.error(error);
            console.groupEnd();
        }
    } else {
        $d(changeInfoItemOnMapAction(null));
    }

    $d(setStatusLoadedInfoObjectAction(false));
};
