import {getGeoTypes, getObjects} from "./objects-reducer";
import {clearAppData} from "./app-reducer";

/**
 *  Редьюсер авторизации
 *
 *
*/

// Названия действий

const
    SET_AUTH = 'auth/SET_AUTH',
    SET_LOGOUT = 'auth/SET_LOGOUT';


// Инициализация

const initialState = {
    token: null,
    isAuth: false,
};


// Reducer

const authReducer = (state = initialState, action) => {

    switch (action.type) {
        case SET_AUTH:
            return {
                ...state,
                isAuth: action.status,
                token: action.token,
            };

        case SET_LOGOUT:
            return {
                ...state,
                isAuth: false,
                token: null,
            };

        default: return state;
    }
};

export default authReducer;


// Actions

export const setAuthAction = (status, token) => ({type: SET_AUTH, status, token});


// Thunks

// Проверка авторизации
export const getAuth = () => async $d => {
    try {
        // TODO: Сделать проверку авторизации
        const
            myToken = localStorage.getItem('mp-token');

            // {data: {ok, user}} = await authAPI.getAuth();

        if (myToken) {
            $d(setAuthAction(true, myToken));

            return true;
        } else {
            throw new Error('Вы неавторизованы');
        }
    } catch(error) {
        console.group('=== Ошибка авторизации ===');
        console.error(error);
        console.groupEnd();

        return false;
    }
};

// Залогиниться
export const login = ({login, password}) => async $d => {
    try {
        //const
        //    {data} = await authAPI.login(login, password);

        const
            data = {token: 'thisistoken'};

        if (data.token) {
            localStorage.setItem('mp-token', data.token);
            localStorage.setItem('mp-login', login);
            $d(getAuth());

            // TODO: Доделать авторизацию
            if (true) {
                const
                    geoTypes = await $d(getGeoTypes()),
                    isLoadedObjects = await $d(getObjects(geoTypes));

                if (!isLoadedObjects) {
                    throw new Error('Данные не загрузились.')
                }
            }
        } else {
            throw new Error('Неправильный логин или пароль.');
        }
    } catch(error) {
        console.group('=== Ошибка авторизации ===');
        console.error(error);
        console.groupEnd();
    }
};

export const logout = () => async $d => {
    localStorage.removeItem('mp-token');
    localStorage.removeItem('mp-login');

    $d(setAuthAction(false, false));
    $d(clearAppData());
};
