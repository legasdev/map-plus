import {appAPI} from "../../api/api";
import toXmlRequest from "../../utils/toXmlRequest";
import xml2object from "../../utils/xml2object";

/**
 *
 *  Редьюсер счетчиков
 *
 */

// Названия действий
const
    CHANGE_OBJECT = 'counters/CHANGE_OBJECT',
    CHANGE_PARAMETERS = 'counters/CHANGE_PARAMETERS',
    CHANGE_STATUS_LIST = 'counters/CHANGE_STATUS_LIST',
    CHANGE_RESOURCE = 'counters/CHANGE_RESOURCE',
    CHANGE_RESOURCE_LIST = 'counters/CHANGE_RESOURCE_LIST';


export const
    COUNTER_STATUS = {
        resource: 'resource',
        showParameters: 'showParameters',
    };

// Инициализация

const initialState = {
    object: null,
    parameters: null,
    statusList: null,
    resourceList: null,
    status: {
        [COUNTER_STATUS.resource]: null,
        [COUNTER_STATUS.showParameters]: false,
    },
};


// Reducer

const countersReducer = (state = initialState, action) => {

    switch (action.type) {

        case CHANGE_OBJECT:
            return {
                ...state,
                object: action.data
            };

        case CHANGE_PARAMETERS:
            return {
                ...state,
                parameters: action.list
            };

        case CHANGE_STATUS_LIST:
            return {
                ...state,
                statusList: action.list,
            };

        case CHANGE_RESOURCE_LIST:
            return {
                ...state,
                resourceList: action.list
            };

        case CHANGE_RESOURCE:
            return {
                ...state,
                status: {
                    ...state.status,
                    [action.status]: action.value
                }
            };

        default: return state;
    }
};

export default countersReducer;


// Actions

export const changeObjectAction = (data) => ({type: CHANGE_OBJECT, data});
export const changeObjectParametersAction = (list) => ({type: CHANGE_PARAMETERS, list});
export const changeStatusListAction = (list) => ({type: CHANGE_STATUS_LIST, list});
export const changeResourceListAction = (list) => ({type: CHANGE_RESOURCE_LIST, list});
export const changeResourceAction = (status, value) => ({type: CHANGE_RESOURCE, status, value});


// Thunks

/**
 * Меняет информацию об объекте на странице счетчиков
 *
 * @param {Object} data - Информация об объекте
 * @returns {Function}
 */
export const changeObjectInfo = (data) => async dispatch => {
    dispatch(changeObjectAction(data));

    if ( !data ) {
        await dispatch(getObjectParameters(0));
        await dispatch(getObjectParameterStatusList(0, null));
        await dispatch(getObjectResourceList(0));
        return;
    }

    const parameters = await dispatch(getObjectInformation(data._ID_Object));
    await dispatch(getObjectParameters(data._ID_Object));
    await dispatch(getObjectParameterStatusList(data._ID_Object, parameters));
    await dispatch(getObjectResourceList(data._ID_Object));

};


/**
 * Получение информации об объекте
 *
 * @param {String|Number} objectId - ID Объекта
 * @returns {Function}
 */
export const getObjectInformation = (objectId = null) => async dispatch => {
    try {

        if (!objectId) {
            throw new Error('Нет ID объекта.');
        }

        const {data} = await appAPI.getData(toXmlRequest({
            operation: 'GEO_Get_Object_Information',
            parameters: [
                {id_object: objectId},
                {parameter: '*'}
            ]
        }));

        if (data) {
            const
                {xml_doc: {response_data: {row: rows}, status}} = xml2object(data);

            if (parseInt(status) < 300) {
                dispatch(changeObjectParametersAction(rows));

                return rows;
            } else {
                throw new Error(`Статус ответа сервера: ${status}`);
            }
        } else {
            throw new Error('Объект с данными - пустой.');
        }
    } catch (error) {
        console.group('===[ Ошибка загрузки информации об объекте ]===');
        console.error(error);
        console.groupEnd();

        return false;
    }
};


/**
 * Загрузка параметров объекта
 *
 * @param {String|Number} objectId - ID Объекта
 * @returns {Function}
 */
export const getObjectParameters = (objectId = null) => async dispatch => {
    try {

        if (!objectId) {
            throw new Error('Нет ID объекта.');
        }

        const {data} = await appAPI.getData(toXmlRequest({
            operation: 'GEO_Get_Object_Parameters',
            parameters: [
                {id_object: objectId}
            ]
        }));

        if (data) {
            const
                {xml_doc: {response_data: {row: rows}, status}} = xml2object(data);

            if (parseInt(status) < 300) {
                dispatch(changeObjectParametersAction(rows));

                return rows;
            } else {
                throw new Error(`Статус ответа сервера: ${status}`);
            }
        } else {
            throw new Error('Объект с данными - пустой.');
        }
    } catch (error) {
        console.group('===[ Ошибка загрузки параметров объекта ]===');
        console.error(error);
        console.groupEnd();

        return false;
    }
};


/**
 * Получение списка параметров объекта с допустимыми значениями и соответствующими статусами
 *
 * @param {String|Number} objectId - ID Объекта
 * @param {Array} parameters - Список параметров
 * @returns {Function}
 */
export const getObjectParameterStatusList = (objectId = null, parameters = null) => async dispatch => {
    try {

        if (!objectId) {
            throw new Error('Нет ID объекта.');
        }

        const {data} = await appAPI.getData(toXmlRequest({
            operation: 'GEO_Get_Object_Parameter_Status_List',
            parameters: [
                {id_object: objectId},
                ...parameters.map(item => ({'parameter': item['Parameter']}))
            ]
        }));

        if (data) {
            const
                {xml_doc: {response_data: {row: rows}, status}} = xml2object(data);

            if (parseInt(status) < 300) {
                dispatch(changeStatusListAction(rows));

                return true;
            } else {
                throw new Error(`Статус ответа сервера: ${status}`);
            }
        } else {
            throw new Error('Объект с данными - пустой.');
        }
    } catch (error) {
        console.group('===[ Ошибка загрузки статусов параметров объекта ]===');
        console.error(error);
        console.groupEnd();

        return false;
    }
};


/**
 * Получение списка ресурсов дочерних элементов объекта
 *
 * @param {String|Number} objectId - ID Объекта
 * @param {Number} recursive - Признак рекурсивности. 0 - только след уровень, 1 - все дочерние уровни рекурсивно
 * @returns {Function}
 */
export const getObjectResourceList = (objectId = null, recursive = 1) => async dispatch => {
    try {

        if (!objectId) {
            throw new Error('Нет ID объекта.');
        }

        const {data} = await appAPI.getData(toXmlRequest({
            operation: 'GEO_Get_Object_Resource_List',
            parameters: [
                {id_object: objectId},
                {recursive}
            ]
        }));

        if (data) {
            const
                {xml_doc: {response_data: {row: rows}, status}} = xml2object(data);

            if (parseInt(status) < 300) {
                dispatch(changeResourceListAction(rows));

                return rows;
            } else {
                throw new Error(`Статус ответа сервера: ${status}`);
            }
        } else {
            throw new Error('Объект с данными - пустой.');
        }
    } catch (error) {
        console.group('===[ Ошибка загрузки списка ресурсов объекта ]===');
        console.error(error);
        console.groupEnd();

        return false;
    }
};
