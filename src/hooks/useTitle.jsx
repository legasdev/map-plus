import {useEffect} from "react";

const useTitle = (title) => {
    useEffect(() => {
        document.title = `${title} / Карта+`
    }, [title]);
};

export default useTitle;
