import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import * as serviceWorker from './serviceWorker';

import './index.css';

import App from './App';
import store from './redux/store';

localStorage.getItem('mp-dark-status') || localStorage.setItem('mp-dark-status', 'false');

if (localStorage.getItem('mp-type-map') !== 'osm') {
	localStorage.setItem('mp-type-map', 'osm');
}

ReactDOM.render(
	<BrowserRouter>
		<Provider store={store}>
			<React.StrictMode>
				<App />
			</React.StrictMode>
		</Provider>
    </BrowserRouter>,
	document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
