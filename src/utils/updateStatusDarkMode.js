/**
 *
 * Обновление темного режима
 *
 */

export default function updateStatusDarkMode(status, typeMap) {
    if (status) {
        document.body.classList.add('dark');

        if (typeMap === 'yandex') {
            document.body.classList.add('dark-yandex');
        } else {
            document.body.classList.remove('dark-yandex');
        }
    } else {
        document.body.classList.remove('dark');
        document.body.classList.remove('dark-yandex');
    }
}