import {appAPI} from "../api/api";
import toXmlRequest from "./toXmlRequest";
import xml2object from "./xml2object";

/**
 * Загружает и возвращает данные по объекту
 *
 * @param objectID
 * @returns {Promise<{kip: *, data: *, odpu: *, objectParam: *, ipu: *}>}
 */
export default async function uploadInfoByObjectID(objectID) {
    try {
        const
            {data} = await appAPI.getData(toXmlRequest({
                operation: 'GEO_Get_Object_Information',
                parameters: [
                    {id_object: objectID},
                    {parameter: '*'}
                ]
            })),
            {data: odpu} = await appAPI.getData(toXmlRequest({
                operation: 'GEO_Get_Group_Objects',
                parameters: [
                    {id_object: objectID},
                    {group_name: 'одпу'}
                ]
            })),
            {data: ipu} = await appAPI.getData(toXmlRequest({
                operation: 'GEO_Get_Group_Objects',
                parameters: [
                    {id_object: objectID},
                    {group_name: 'ипу'}
                ]
            })),
            {data: kip} = await appAPI.getData(toXmlRequest({
                operation: 'GEO_Get_Group_Objects',
                parameters: [
                    {id_object: objectID},
                    {group_name: 'кип'}
                ]
            })),
            {data: objectParam} = await appAPI.getData(toXmlRequest({
                operation: 'GEO_Get_Object_Parameters',
                parameters: [
                    {id_object: objectID},
                    {parameter: '*'},
                    {type_params: '1'}
                ]
            }));

        try {
            const
                {xml_doc: {response_data}} = xml2object(data),
                {xml_doc: {response_data: {row: odpuData}}} = xml2object(odpu),
                {xml_doc: {response_data: {row: ipuData}}} = xml2object(ipu),
                {xml_doc: {response_data: {row: kipData}}} = xml2object(kip),
                {xml_doc: {response_data: {row: objectParameters}}} = xml2object(objectParam);

            const
                odpuDataArray = odpuData && (Array.isArray(odpuData) ? odpuData : [odpuData]),
                ipuDataArray = ipuData && (Array.isArray(ipuData) ? ipuData : [ipuData]),
                kipDataArray = kipData && (Array.isArray(kipData) ? kipData : [kipData]);

            return ({
                data: Array.isArray(response_data.row) ? {row: response_data.row} : {row: [response_data.row]},
                odpu: {
                    data: odpuDataArray,
                    badCount: odpuDataArray
                        ? odpuDataArray.reduce((num, item) =>
                            parseInt(item['_ID_Object_Status']) === 1 ? num + 1 : num, 0)
                        : 0,
                    warnCount: odpuDataArray
                        ? odpuDataArray.reduce((num, item) =>
                            parseInt(item['_ID_Object_Status']) === 3 ? num + 1 : num, 0)
                        : 0,
                },
                ipu: {
                    data: ipuDataArray,
                    badCount: ipuDataArray
                        ? ipuDataArray.reduce((num, item) =>
                            parseInt(item['_ID_Object_Status']) === 1 ? num + 1 : num, 0)
                        : 0,
                    warnCount: ipuDataArray
                        ? ipuDataArray.reduce((num, item) =>
                                parseInt(item['_ID_Object_Status']) === 3 || parseInt(item['_ID_Object_Status']) === 10
                                    ? num + 1
                                    : num
                            , 0)
                        : 0,
                },
                kip: {
                    data: kipDataArray,
                    badCount: kipDataArray
                        ? kipDataArray.reduce((num, item) =>
                            parseInt(item['_ID_Object_Status']) === 1 ? num + 1 : num, 0)
                        : 0,
                    warnCount: kipDataArray
                        ? kipDataArray.reduce((num, item) =>
                            parseInt(item['_ID_Object_Status']) === 3 ? num + 1 : num, 0)
                        : 0,
                },
                objectParameters,
            });
        } catch (error) {
            console.group(`===[ Ошибка преобразования информации об объекте. ObjectID: ${objectID} ]===`);
            console.error(error);
            console.groupEnd();
        }
    } catch (error) {
        console.group(`===[ Ошибка загрузки информации об объекте. ObjectID: ${objectID} ]===`);
        console.error(error);
        console.groupEnd();
    }

    return null;
}
