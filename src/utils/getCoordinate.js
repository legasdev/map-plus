export function getCoordinatePoint(object) {
    return object['_Coordinate']
        ? object['_Coordinate'].replace(/\(/g, '').replace(/\)/g, '').split(' ')
        : ['',0,0];
}

export function getCoordinatePolyline(coordinate) {
    return coordinate
        .filter((_, i) => i !== 0)
        .map(pos => parseFloat(pos))
        .reduce((acc, pos, i, array) => {
            if (i % 2 === 0) {
                return [...acc, [pos, array[i+1]]];
            } else {
                return acc;
            }
        }, []);
}
