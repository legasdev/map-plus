/**
 * Поиск объекта в системе по его ID
 *
 * @param {Number|String} objectId - ID объекта в системе
 * @param {Array|Object} objects - Массив или объект с объектами в системе
 *
 * @returns {Object|Null} - Объект или Null, если не найден объект
 */
export default function findObjectByObjectId(objectId, objects) {
    try {
        if (!objects || !objectId) {
            return null;
        }

        const
            objectsArray = Array.isArray(objects)
                ? [...objects]
                : Object.values(objects).reduce((array, item) => {
                    return [...array, ...item.objects]
                }, []);

        return objectsArray.find(item => {
            return item['_ID_Object'] === objectId
        });
    } catch(error) {
        console.group('=== Ошибка нахождения элемента ===');
        console.error(error);
        console.groupEnd();
    }

    return null;
}
