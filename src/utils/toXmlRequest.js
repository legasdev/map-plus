/**
 * Преобразует объект в XML
 *
 * @param {Object} request
 * @returns {string}
 */
export default function toXmlRequest(request={}) {

    const
        data = Object.keys(request).map(key => {
            if (key === 'parameters') {
                const
                    innerParameter = request['parameters']
                        .map(object =>
                            Object
                                .keys(object)
                                .map(field => `<${field}>${object[field]}</${field}>`))
                        .flat()
                        .join('');

                return `<parameters>${innerParameter}</parameters>`;
            }

            return `<${key}>${request[key]}</${key}>`
        }).join('');

    return `<?xml version="1.0" encoding="UTF-8"?><xml_doc>${data}</xml_doc>`;
};