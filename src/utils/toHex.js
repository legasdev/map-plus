/**
 * Перевод в HEX цвет
 *
 * @param number
 * @returns {string}
 */

export default function decimalToHexString(number) {
    if (number < 0)
    {
        number = 0xFFFFFFFF + number + 1;
    }

    const
        hexString = number.toString(16).toUpperCase();

    return hexString.length < 6 ? new Array(6 - hexString.length).fill('0').join('') + hexString : hexString;
}