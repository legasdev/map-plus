export default function getDate(plusDays=0) {
    const
        date = new Date();

    date.setDate(date.getDate() + plusDays);

    const
        year = date.getFullYear(),
        month = (date.getMonth() + 1) < 10 ? `0${date.getMonth() + 1}` : date.getMonth() + 1,
        day = date.getDate() < 10 ?  `0${date.getDate()}` : date.getDate();

    return `${year}-${month}-${day}`;
}