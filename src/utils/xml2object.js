import xml2json from "./xml2json";

export default function xml2object(xml) {
    const
        parserXML = new DOMParser();
    return JSON.parse(xml2json(parserXML.parseFromString(xml.replace(/&quot;/g, '\\&quot;'), 'text/xml'), ''));
};