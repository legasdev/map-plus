/**
 *
 *  Типы таблиц
 *
 */

export default {
    'ipu': 'ИПУ',
    'odpu': 'ОДПУ',
    'kip': 'КИП',
};