import L from "leaflet";

const
    pathToIcons = `/img/icons/map/objects`,
    pathToHoverIcons = `/img/icons/map/hover-objects`;

const options = {
    iconAnchor: [22, 55],
    iconSize: [44, 55],
    popupAnchor: [0, -45],
    shadowUrl: `${pathToIcons}/marker-shadow.png`,
    shadowSize: [22, 55],
    shadowAnchor: [6, 60],
};
const optionsHover = {
    iconAnchor: [27, 65],
    iconSize: [54, 65],
    popupAnchor: [0, -55],
    shadowUrl: `${pathToIcons}/marker-shadow.png`,
    shadowSize: [27, 65],
    shadowAnchor: [5, 70],
};

// Котельная [ID:1]
const boilerRoomIcon = new L.Icon({
    iconUrl: `${pathToIcons}/ic_marker_boilerRoom.svg`,
    iconRetinaUrl: `${pathToIcons}/ic_marker_boilerRoom.svg`,
    ...options
});

const boilerRoomIconHover = new L.Icon({
    iconUrl: `${pathToHoverIcons}/ic_marker_boilerRoom.svg`,
    iconRetinaUrl: `${pathToHoverIcons}/ic_marker_boilerRoom.svg`,
    ...optionsHover
});

// ИТП [ID:3]
const itpIcon = new L.Icon({
    iconUrl: `${pathToIcons}/ic_marker_itpStation.svg`,
    iconRetinaUrl: `${pathToIcons}/ic_marker_itpStation.svg`,
    ...options,
    iconAnchor: [22, 49],
});

const itpIconHover = new L.Icon({
    iconUrl: `${pathToHoverIcons}/ic_marker_itpStation.svg`,
    iconRetinaUrl: `${pathToHoverIcons}/ic_marker_itpStation.svg`,
    ...optionsHover,
    iconAnchor: [27, 60],
});

// Дом [ID:4]
const homeIcon = new L.Icon({
    iconUrl: `${pathToIcons}/ic_marker_home.svg`,
    iconRetinaUrl: `${pathToIcons}/ic_marker_home.svg`,
    ...options
});

const homeIconHover = new L.Icon({
    iconUrl: `${pathToHoverIcons}/ic_marker_home.svg`,
    iconRetinaUrl: `${pathToHoverIcons}/ic_marker_home.svg`,
    ...optionsHover
});

// Теплосчетчик [ID:5]
const heatMeterIcon = new L.Icon({
    iconUrl: `${pathToIcons}/ic_marker_heatMeter.svg`,
    iconRetinaUrl: `${pathToIcons}/ic_marker_heatMeter.svg`,
    ...options
});

const heatMeterIconHover = new L.Icon({
    iconUrl: `${pathToHoverIcons}/ic_marker_heatMeter.svg`,
    iconRetinaUrl: `${pathToHoverIcons}/ic_marker_heatMeter.svg`,
    ...optionsHover
});

// Энергосчетчик [ID:6]
const energyMeterIcon = new L.Icon({
    iconUrl: `${pathToIcons}/ic_marker_energyMeter.svg`,
    iconRetinaUrl: `${pathToIcons}/ic_marker_energyMeter.svg`,
    ...options
});

const energyMeterIconHover = new L.Icon({
    iconUrl: `${pathToHoverIcons}/ic_marker_energyMeter.svg`,
    iconRetinaUrl: `${pathToHoverIcons}/ic_marker_energyMeter.svg`,
    ...optionsHover
});

// Электрическая подстанция [ID:10]
const powerSubstationIcon = new L.Icon({
    iconUrl: `${pathToIcons}/ic_marker_powerSubstation.svg`,
    iconRetinaUrl: `${pathToIcons}/ic_marker_powerSubstation.svg`,
    ...options,
    iconAnchor: [22, 49],
});

const powerSubstationIconHover = new L.Icon({
    iconUrl: `${pathToHoverIcons}/ic_marker_powerSubstation.svg`,
    iconRetinaUrl: `${pathToHoverIcons}/ic_marker_powerSubstation.svg`,
    ...optionsHover,
    iconAnchor: [27, 60],
});

// Запорная арматура [ID:11]
const craneIcon = new L.Icon({
    iconUrl: `${pathToIcons}/ic_marker_craneIcon.svg`,
    iconRetinaUrl: `${pathToIcons}/ic_marker_craneIcon.svg`,
    ...options,
    iconAnchor: [22, 49],
});

const craneIconHover = new L.Icon({
    iconUrl: `${pathToHoverIcons}/ic_marker_craneIcon.svg`,
    iconRetinaUrl: `${pathToHoverIcons}/ic_marker_craneIcon.svg`,
    ...optionsHover,
    iconAnchor: [27, 60],
});

// Водонапорная станция [ID:12]
const waterStationIcon = new L.Icon({
    iconUrl: `${pathToIcons}/ic_marker_waterStation.svg`,
    iconRetinaUrl: `${pathToIcons}/ic_marker_waterStation.svg`,
    ...options,
    iconAnchor: [22, 49],
});

const waterStationIconHover = new L.Icon({
    iconUrl: `${pathToHoverIcons}/ic_marker_waterStation.svg`,
    iconRetinaUrl: `${pathToHoverIcons}/ic_marker_waterStation.svg`,
    ...optionsHover,
    iconAnchor: [27, 60],
});

// Электрическая станция [ID:14]
const powerStationIcon = new L.Icon({
    iconUrl: `${pathToIcons}/ic_marker_powerStation.svg`,
    iconRetinaUrl: `${pathToIcons}/ic_marker_powerStation.svg`,
    ...options,
    iconAnchor: [22, 49],
});

const powerStationIconHover = new L.Icon({
    iconUrl: `${pathToHoverIcons}/ic_marker_powerStation.svg`,
    iconRetinaUrl: `${pathToHoverIcons}/ic_marker_powerStation.svg`,
    ...optionsHover,
    iconAnchor: [27, 60],
});

// Счетчик холодной воды [ID:15]
const coldWaterMeterIcon = new L.Icon({
    iconUrl: `${pathToIcons}/ic_marker_coldWaterMeter.svg`,
    iconRetinaUrl: `${pathToIcons}/ic_marker_coldWaterMeter.svg`,
    ...options
});

const coldWaterMeterIconHover = new L.Icon({
    iconUrl: `${pathToHoverIcons}/ic_marker_coldWaterMeter.svg`,
    iconRetinaUrl: `${pathToHoverIcons}/ic_marker_coldWaterMeter.svg`,
    ...optionsHover
});

// Теплосчетчик ТВ7 [ID:19]
const heatTV7MeterIcon = new L.Icon({
    iconUrl: `${pathToIcons}/ic_marker_heatTV7Meter.svg`,
    iconRetinaUrl: `${pathToIcons}/ic_marker_heatTV7Meter.svg`,
    ...options
});

const heatTV7MeterIconHover = new L.Icon({
    iconUrl: `${pathToHoverIcons}/ic_marker_heatTV7Meter.svg`,
    iconRetinaUrl: `${pathToHoverIcons}/ic_marker_heatTV7Meter.svg`,
    ...optionsHover
});

// Дефолтная [ID: Null]
const defaultIcon = new L.Icon({
    iconUrl: `${pathToIcons}/ic_marker_default.svg`,
    iconRetinaUrl: `${pathToIcons}/ic_marker_default.svg`,
    ...options
});

const defaultIconHover = new L.Icon({
    iconUrl: `${pathToHoverIcons}/ic_marker_default.svg`,
    iconRetinaUrl: `${pathToHoverIcons}/ic_marker_default.svg`,
    ...optionsHover
});

export default {
    default: defaultIcon,
    '1': boilerRoomIcon,
    '3': itpIcon,
    '4': homeIcon,
    '5': heatMeterIcon,
    '6': energyMeterIcon,
    '10': powerSubstationIcon,
    '11': craneIcon,
    '12': waterStationIcon,
    '14': powerStationIcon,
    '15': coldWaterMeterIcon,
    '19': heatTV7MeterIcon,
    'default_hover': defaultIconHover,
    '1_hover': boilerRoomIconHover,
    '3_hover': itpIconHover,
    '4_hover': homeIconHover,
    '5_hover': heatMeterIconHover,
    '6_hover': energyMeterIconHover,
    '10_hover': powerSubstationIconHover,
    '11_hover': craneIconHover,
    '12_hover': waterStationIconHover,
    '14_hover': powerStationIconHover,
    '15_hover': coldWaterMeterIconHover,
    '19_hover': heatTV7MeterIconHover,
}
