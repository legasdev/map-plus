import React, {memo, useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {NavLink, Route, Switch, withRouter} from "react-router-dom";
import {LastLocationProvider} from "react-router-last-location";

import './assets/styles/index.less';
import './App.less';

import {setInitialApp} from "./redux/reducers/app-reducer";
import LoginPage from "./components/pages/Login";
import Home from "./components/Home";
import MapPage from "./components/pages/Map";
import InformationPage from "./components/pages/Information";

const App = (props) => {

    const
        $d = useDispatch();

    const
        isInitialApp = useSelector(state => state.app.status.initialApp),
        isDarkStyle = useSelector(state => state.app.status.isDarkStyle),
        isAuth = useSelector(state => state.auth.isAuth);

    useEffect(() => {
        if (!isInitialApp) {
            $d(setInitialApp());
        }
    }, [$d, isInitialApp]);

    return isInitialApp
        ? isAuth
            ? (<div className={'App'}>
                <LastLocationProvider watchOnlyPathname>
                    <Switch>
                        <Route
                            path={'/map'}
                            render={ () => <MapPage /> }
                        />
                        <Route
                            exact
                            path={'/:group/:objectId'}
                            render={ () => <InformationPage /> }
                        />
                        <Route
                            exact
                            path={'/:group/:objectId/:counterId'}
                            render={ () => <InformationPage /> }
                        />
                        <Route
                            render={ () => <Home /> }
                        />
                    </Switch>
                </LastLocationProvider>
                <NavLink className={'logo'} to={'/'}>
                    <img src={isDarkStyle ? '/img/logo-dark.png' : '/img/logo.png'} alt={'Go to home page'} />
                </NavLink>
            </div>)
            : <LoginPage />
        : <p>Загрузка...</p>;
};

export default memo(withRouter(App));
