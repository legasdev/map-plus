import Axios from 'axios';

const Instance = Axios.create({
    baseURL: 'http://37.18.74.11:7443/demo_geo/',
    // baseURL: 'http://212.46.211.20:50000/geo_guk/',
    withCredentials: false,
});

export default Instance;