import Instance from './instance';

/**
 * Добавляет токен к заголовкам
 *
 * @param options - Объект заголовков запроса
 * @returns {{Authorization: string}}
 */
function addToken(options={}) {
    return {
        ...options,
        // 'Authorization': `Bearer ${localStorage.getItem('token')}`
    };
}

export const appAPI = {

    async getData(data) {
        return await Instance.post('', data, {
            headers: addToken()
        });
    }

};