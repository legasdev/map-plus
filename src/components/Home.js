import React from "react";
import {Redirect} from "react-router-dom";

import useTitle from "./../hooks/useTitle";

const Home = (props) => {

    useTitle('Редирект...');

    return <Redirect to={'/map'} />
};

export default Home;
