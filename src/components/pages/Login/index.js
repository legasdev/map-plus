import React, {useCallback} from "react";
import {useDispatch} from "react-redux";

import s from './style.module.less';

import {login} from "../../../redux/reducers/auth-reducer";
import useTitle from "../../../hooks/useTitle";

const LoginPage = (props) => {

    const
        $d = useDispatch();

    const
        onClickLogin = useCallback(() => {
            // TODO: Fixed login data
            $d(login({login: 'admin', password: 'admin'}));
        }, [$d]);

    useTitle('Авторизация');

    return (
        <div className={s.content}>
            <button className={'button button--styles'} onClick={onClickLogin}>Войти</button>
        </div>
    )
};

export default LoginPage;
