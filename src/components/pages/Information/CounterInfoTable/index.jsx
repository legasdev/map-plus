import React, {useEffect, useMemo, useState} from "react";
import {DataType, SortingMode} from "ka-table/enums";
import {useDispatch, useSelector} from "react-redux";
import {kaReducer, Table} from "ka-table";

import s from "./CounterInfoTable.module.less";

import {changeResourceAction, COUNTER_STATUS} from "@src/redux/reducers/counters-reducer";
import ParametersList from "@src/components/common/ParametersList";

const CounterInfoTable = ({ data, currentDate, counterParams, handleChangeDate, selectedParams, updateSelectedParams }) => {

    const
        dispatch = useDispatch();

    const
        isShowParameters = useSelector(state => state.counters.status[COUNTER_STATUS.showParameters]);

    const childAttributes = {
        dataRow: {
            elementAttributes: (props) => ({
                title: `${props.rowData.name}`,
            }),
        },
    };

    const
        parametersDate = useMemo(() => {
            return counterParams?.reduce((acc, item) => {
                if ( !item ) {
                    return acc;
                }

                return {
                    ...acc,
                    [item['Parameter']]: {
                        name: item['Display_Name']
                    }
                };
            }, {});
        }, [counterParams]);

    const dataArray = useMemo(() => {
        return data?.map((item) => ({
            interval: parseFloat(item['Interval_value']),
            ...(selectedParams ? selectedParams?.reduce((obj, param) => ({
                ...obj,
                [param]: item[param] || 0
            }), {}) : {}),
        }));
    }, [data, selectedParams]);

    const tablePropsInit = useMemo(() => ({
        columns: parametersDate ? [
            { key: 'interval', title: 'День', dataType: DataType.Number },
            ...(selectedParams ? selectedParams.map(param => ({
                key: param, title: parametersDate[param].name || '', dataType: DataType.String
            })) : []),
        ] : [
            { key: 'interval', title: 'День', dataType: DataType.Number },
        ],
        data: dataArray,
        rowKeyField: 'interval',
        sortingMode: SortingMode.Single,
    }), [dataArray, selectedParams, parametersDate]);

    const
        [tableProps, changeTableProps] = useState(tablePropsInit);

    const
        handleChangeParameters = (parameter) => {
            const
                option = selectedParams.find(selectParam => selectParam === parameter);

            if (option) {
                updateSelectedParams(selectedParams.filter(selectParam => selectParam !== parameter));
            } else {
                updateSelectedParams([...selectedParams, parameter]);
            }
        };

    const dispatchTable = (action) => { // dispatch has an *action as an argument
        // *kaReducer returns new *props according to previous state and *action, and saves new props to the state
        changeTableProps((prevState) => kaReducer(prevState, action));
    };

    const
        handleClickOpenParams = () => {
            dispatch(changeResourceAction(COUNTER_STATUS.showParameters, true));
        };

    useEffect(() => {
        changeTableProps(tablePropsInit);
    }, [tablePropsInit]);

    return (
        <div className={`${s.main} hover-row`}>
            <header className={s.header}>
                <div className={s.input_date}>
                    <label htmlFor="counterDate">Выбранная дата: </label>
                    <input id="counterDate" type="date" value={currentDate} onChange={handleChangeDate} />
                </div>
                <button className={s.paramButton} onClick={handleClickOpenParams}>Параметры</button>
            </header>
            {
                data?.length && parametersDate
                    ? <Table
                        {...tableProps}
                        childComponents={childAttributes}
                        dispatch={dispatchTable}
                    />
                    : <span>Данные не были загружены. Пожалуйста, обновите страницу.</span>
            }
            {
                isShowParameters &&
                <ParametersList
                    counterParams={counterParams}
                    selectedParams={selectedParams}
                    handleChangeParameters={handleChangeParameters}
                />
            }
        </div>
    );
};

export default CounterInfoTable;