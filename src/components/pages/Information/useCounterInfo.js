import {useEffect, useMemo, useState} from "react";
import {appAPI} from "@src/api/api";
import toXmlRequest from "@src/utils/toXmlRequest";
import xml2object from "@src/utils/xml2object";

function useCounterInfo(counterId, objects, currentDate, selectedParams) {
    const
        [counterParams, updateCounterParams] = useState(null),
        [counterData, updateCounterData] = useState(null);

    const
        [year, month, day] = useMemo(() => {
            return currentDate.split('-');
        }, [currentDate]);

    const
        counterObject = useMemo(() => {
            const
                objectsList = Object
                    .values(objects)
                    .map(objectsByType => objectsByType.objects)
                    .flat();

            return objectsList.find(object => object['_ID_Object'] === counterId) || null;
        }, [objects, counterId]);

    useEffect(() => {
        if ( !counterId ) return;

        (async () => {
            try {
                const {data} = await appAPI.getData(toXmlRequest({
                    operation: 'GEO_Get_Object_Parameters',
                    parameters: [
                        {id_object: counterId},
                    ]
                }));

                if (data) {
                    const
                        {xml_doc: {response_data: {row: rows}, status}} = xml2object(data);

                    if (parseInt(status) < 300) {
                        if ( !rows ) {
                            updateCounterParams(null);
                            return;
                        }

                        updateCounterParams(Array.isArray(rows) ? rows : [rows]);
                    } else {
                        throw new Error(`Статус ответа сервера: ${status}`);
                    }
                } else {
                    throw new Error('Объект с данными - пустой.');
                }
            } catch (error) {
                console.error(error);
                updateCounterParams([]);
            }
        })();

    }, [counterId]);

    useEffect(() => {
        if ( !counterId ) return;

        (async () => {
            try {
                const {data} = await appAPI.getData(toXmlRequest({
                    operation: 'GEO_Get_Object_History',
                    parameters: [
                        {id_object: counterId},
                        {interval: 'month'},
                        {interval_day: day},
                        {interval_month: month},
                        {interval_year: year},
                        ...selectedParams.map(param => ({parameter: param})),
                    ]
                }));

                if (data) {
                    const
                        {xml_doc: {response_data: {row: rows}, status}} = xml2object(data);

                    if (parseInt(status) < 300) {
                        updateCounterData(rows);
                    } else {
                        console.warn('rows', rows)
                        throw new Error(`Статус ответа сервера: ${status}`);
                    }
                } else {
                    throw new Error('Объект с данными - пустой.');
                }
            } catch (error) {
                console.error(error);
                updateCounterParams([]);
            }
        })();
    }, [counterId, selectedParams, year, month, day]);

    return [counterObject, counterParams, counterData];
}

export default useCounterInfo;