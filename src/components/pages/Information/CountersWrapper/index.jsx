import React, {useMemo} from "react";
import {useSelector} from "react-redux";

import s from './style.module.less';

import CountersTable from "./CountersTable";
import {COUNTER_STATUS} from "@src/redux/reducers/counters-reducer";
import {useParams} from "react-router-dom";
import typeTables from "@src/settings/typeTables";

const CountersWrapper = () => {

    const
        { group, objectId } = useParams();

    const
        countersInState = useSelector(state => state.objects.objects),
        activeResource = useSelector(state => state.counters.status[COUNTER_STATUS.resource]);

    const
        counters = useMemo(() => Object.values(countersInState)
                .map(item => item.objects)
                .flat()
                .filter(item => {
                    const type = item['ID_Object_Node_Type'];

                    return type === '90' || type === "100";
                })
                .filter(item => item?._ID_Parent === objectId)
                .filter(item => item?.Группа === typeTables[group])
                .filter(item => activeResource
                    ? item['_ID_Object_Resource'] === activeResource
                    : true),
            [countersInState, activeResource, group, objectId]);

    return (
        <div className={s.main}>
            <CountersTable counters={counters} />
            <div />
        </div>
    );
};

export default CountersWrapper;
