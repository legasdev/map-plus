import React, {memo, useEffect, useMemo, useState} from "react";
import {useHistory, useParams} from "react-router-dom";

import {kaReducer, Table} from 'ka-table';
import { DataType, SortingMode } from 'ka-table/enums';

import "ka-table/style.css";
import s from './style.module.less';
import './table.less';

const ROW_MOUSE_CLICK = 'ROW_MOUSE_CLICK';

const CountersTable = ({ counters=[] }) => {

    const
        { group, objectId } = useParams(),
        history = useHistory();

    const childAttributes = {
        dataRow: {
            elementAttributes: (props) => ({
                title: `${props.rowData.name}`,
                onClick: (event, extendedEvent) => {
                    const {
                        childProps: {
                            rowKeyValue,
                        },
                        dispatch,
                    } = extendedEvent;
                    dispatch({ type: ROW_MOUSE_CLICK, rowKeyValue });
                },
            }),
        },
    };

    const dataArray = useMemo(() => {
        return counters.map((item) => ({
            id: item['_ID_Object'],
            name: item['Наименование'],
            group: item['Группа'],
            resource: item['Ресурс'],
            type: item['Тип'],
        }));
    }, [counters]);

    const tablePropsInit = useMemo(() => ({
        columns: [
            { key: 'id', title: 'ID', dataType: DataType.String },
            { key: 'name', title: 'Наименование', dataType: DataType.String },
            { key: 'group', title: 'Группа', dataType: DataType.String },
            { key: 'resource', title: 'Ресурс', dataType: DataType.String },
            { key: 'type', title: 'Тип', dataType: DataType.String },
        ],
        data: dataArray,
        rowKeyField: 'id',
        sortingMode: SortingMode.Single,
    }), [dataArray]);

    const
        [tableProps, changeTableProps] = useState(tablePropsInit);

    const dispatch = (action) => { // dispatch has an *action as an argument
        if (action.type === ROW_MOUSE_CLICK) {
            const obj = dataArray.find((i) => i.id === action.rowKeyValue);
            history.push(`/${group}/${objectId}/${obj.id}`);
        }
        // *kaReducer returns new *props according to previous state and *action, and saves new props to the state
        changeTableProps((prevState) => kaReducer(prevState, action));
    };

    useEffect(() => {
        changeTableProps(tablePropsInit);
    }, [tablePropsInit]);

    return (
        <div className={`${s.main} hover-row`}>
            {
                counters?.length
                ? <Table
                    {...tableProps}
                    childComponents={childAttributes}
                    dispatch={dispatch}
                />
                : <span>Счетчиков в данной группе нет.</span>
            }
        </div>
    );
};

export default memo(CountersTable);
