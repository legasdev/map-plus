import React, {useCallback, useEffect, useMemo, useState} from "react";
import {useParams} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";

import useTitle from "@src/hooks/useTitle";

import s from "./style.module.less";

import {changeObjectInfo} from "@src/redux/reducers/counters-reducer";
import findObjectByObjectId from "@src/utils/findObjectByObjectId";

import AsidePanel from "@src/components/common/Aside";
import LeftPanel from "@src/components/common/LeftPanel";
import SettingsPopup from "@src/components/common/SettingsPopup";
import MapInfoItem from "./../Map/MapInfoItem";
import Header from "../../common/Header";
import CountersWrapper from "./CountersWrapper";
import ResourceNavigation from "../../common/ResourceNavigation";
import useCounterInfo from "@src/components/pages/Information/useCounterInfo";
import CounterInfoTable from "@src/components/pages/Information/CounterInfoTable";

function maskDate(date) {
    const dateObj = new Date(date);

    return `${dateObj.getUTCFullYear()}-${dateObj.getMonth() + 1}-${dateObj.getDate()}`
}

const InformationPage = props => {

    const
        { group, objectId, counterId } = useParams();

    const
        $d = useDispatch();

    const
        objects = useSelector(state => state.objects.objects),
        object = useSelector(state => state.counters.object);

    const
        [selectedParams, updateSelectedParams] = useState([]),
        // [currentDate, setCurrentDate] = useState(maskDate(new Date()));
        [currentDate, setCurrentDate] = useState('2020-03-25');

    const
        [counterObject, counterParams, counterData] = useCounterInfo(counterId, objects, currentDate, selectedParams);

    const
        titleName = useMemo(() => {
            const pageType = group === 'odpu'
                ? 'ОДПУ'
                : group === 'ipu'
                    ? 'ИПУ'
                    : group === 'kip'
                        ? 'КИП'
                        : '';

            if ( !object ) {
                return 'Объект не найден';
            }

            const
                objectName = object && object['_ID_Object'] === objectId
                    ? object['Наименование']
                    : 'Загрузка объекта...';

            const
                counterName = counterObject && counterObject['Наименование'];

            return `${counterName ? `${counterName} / ` : ''}${objectName} / ${pageType}`;
        }, [object, counterObject, group, objectId]);

    const
        handleChangeDate = useCallback((event) => {
            setCurrentDate(event.target.value);
        }, []);

    useTitle(titleName);

    useEffect(() => {
        if (!object || object['_ID_Object'] !== objectId) {
            $d(changeObjectInfo(findObjectByObjectId(objectId, objects)));
        }
    }, [object, objects, group, objectId, $d]);

    console.log('object', object)

    return (
        <div className={s.content}>
            <AsidePanel />
            <LeftPanel/>
            <SettingsPopup />
            <MapInfoItem/>
            <div className={s.wrapper}>
                {
                    object
                        ? <>
                            <Header
                                name={object['Наименование']}
                                counterName={counterObject && counterObject['Наименование']}
                                type={object['Тип']}
                                address={object['Адрес']}
                                link={{group, objectId}}
                            />
                            {
                                !counterId
                                    ?
                                    <>
                                        <ResourceNavigation />
                                        <CountersWrapper />
                                    </>
                                    :
                                    <>
                                        <CounterInfoTable
                                            currentDate={currentDate}
                                            handleChangeDate={handleChangeDate}
                                            data={counterData}
                                            counterParams={counterParams}
                                            selectedParams={selectedParams}
                                            updateSelectedParams={updateSelectedParams}
                                        />
                                    </>
                            }
                        </>
                        : <span style={{margin: '2rem'}}>Объект не найден</span>
                }
            </div>
        </div>
    );
};

export default InformationPage;
