import React from "react";

import s from './style.module.less';

import useTitle from "./../../../hooks/useTitle";
import MapInst from "./MapInst";
import MapStatus from "./MapStatus";
import AsidePanel from "./../../common/Aside";
import LeftPanel from "./../../common/LeftPanel";
import SettingsPopup from "./../../common/SettingsPopup";
import MapInfoItem from "./MapInfoItem";

const MapPage = props => {

    useTitle('Карта');

    return (
        <div className={s.content}>
            <AsidePanel />
            <MapInst />
            <LeftPanel/>
            <SettingsPopup />
            <MapStatus />
            <MapInfoItem/>
        </div>
    );
};

export default MapPage;
