import React, {useCallback} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Marker, Popup, Polyline, useMap, Tooltip} from "react-leaflet";

import settings from "@src/settings/settings";
import icons from "@src/settings/icons";
import {getCoordinatePoint, getCoordinatePolyline} from "@src/utils/getCoordinate";
import {changeChosenMarkerAction, openMapInfoItem} from "@src/redux/reducers/app-reducer";

const MapPointManager = ({objects={}}) => {

    const map = useMap();

    const dispatch = useDispatch();

    const
        filterList = useSelector(state => state.app.filterList),
        chosenMarker = useSelector(state => state.app.chosenMarker);

    const
        onClickMarker = useCallback((event, objectType, object) => {
            chosenMarker && (chosenMarker.target.setIcon(icons[chosenMarker.type] || icons.default));
            chosenMarker && (chosenMarker.target.setZIndexOffset(0));
            dispatch(changeChosenMarkerAction({
                target: event.target,
                latlng: event.latlng,
                type: objectType,
            }));

            dispatch(openMapInfoItem(true, object['_ID_Object'], {
                name: object['Наименование'],
                address: object['Адрес'],
                IDObjectNodeType: object['ID_Object_Node_Type'],
            }));

            event.target.setIcon(icons[`${objectType}_hover`] || icons['default_hover']);
            event.target.setZIndexOffset(200000);
            map.flyTo(event.latlng, 18);
        }, [map, chosenMarker, dispatch]),

        onMouseoverMarker = useCallback((event, objectType) => {
            event.target.setIcon(icons[`${objectType}_hover`] || icons['default_hover']);
            event.target.setZIndexOffset(200000);
        }, []),

        onMouseoutMarker = useCallback((event, objectType) => {
            if (chosenMarker?.latlng.lat ===  event.latlng.lat && chosenMarker?.latlng.lng ===  event.latlng.lng
                && chosenMarker?.type === objectType) {
                return;
            }

            event.target.setIcon(icons[objectType] || icons.default);
            event.target.setZIndexOffset(0);
        }, [chosenMarker]);

    return objects && Object
        .values(objects)
        .reduce((acc, objType) => {
            if (objType.name === 'Дом') {
                return [
                    ...acc,
                    objType,
                ];
            } else {
                return [
                    objType,
                    ...acc,
                ];
            }
        }, [])
        .map(objectsByType => {
            return objectsByType
                .objects
                .filter(object => filterList ? object['_ID_Object_Status'] === filterList : true)
                .map(object => {
                    const
                        coordinate = getCoordinatePoint(object),
                        objectType = object['_ID_Object_Type'];

                    if (coordinate[0].toLowerCase() === 'point') {
                        const
                            icon = icons[objectType] || icons.default;

                        return <Marker
                            key={object['_ID_Object']}
                            position={[parseFloat(coordinate[1]), parseFloat(coordinate[2])]}
                            icon={icon}
                            eventHandlers={{
                                click: (event) => onClickMarker(event, objectType, object),
                                mouseover: (event) => onMouseoverMarker(event, objectType),
                                mouseout: (event) => onMouseoutMarker(event, objectType),
                            }}
                        >
                            <Tooltip offset={[0, -5]} sticky>{object['Наименование']}</Tooltip>
                        </Marker>;

                    } else if (coordinate[0].toLowerCase() === 'linestring') {
                        // Вывод труб
                        const
                            polyLineCoordinate = getCoordinatePolyline(coordinate),
                            color = settings.colorsPipe[objectType] || '#2b2b2b';

                        return <Polyline
                            key={object['_ID_Object']}
                            pathOptions={{color}}
                            positions={polyLineCoordinate}
                        >
                            <Popup>
                                <p>Наименование: {object['Наименование']}</p>
                                <p>Тип: {object['Тип']}</p>
                                <p>Адрес: {object['Адрес']}</p>
                                <p>Статус: {object['Статус']}</p>
                            </Popup>
                        </Polyline>;
                    }

                    return null;
                })
            });
};

export default MapPointManager;
