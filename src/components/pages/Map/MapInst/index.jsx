import React, {useState} from "react";
import {useSelector} from "react-redux";

import { MapContainer } from "react-leaflet";
import {CRS} from 'leaflet';

import s from './style.module.less';

import MapPointManager from "./MapPointManager";
import MapLayersControl from "./MapLayersControl";

const MapInst = ({props}) => {

    const
        isDarkModeActive = useSelector(state => state.app.status.isDarkStyle),
        typeMap = useSelector(state => state.app.status.typeMap),
        objects = useSelector(state => state.objects.objects),
        positionMap = useSelector(state => state.app.settings.positionMap),
        zoom = useSelector(state => state.app.settings.zoom);

    const
        [viewportMap] = useState({
            center: positionMap,
            zoom
        });

    return (
        <div className={s.container}>
            <MapContainer
                center={viewportMap.center}
                zoom={viewportMap.zoom}
                crs={typeMap === 'yandex' ? CRS.EPSG3395 : CRS.EPSG3857 }
            >
                <MapLayersControl
                    typeMap={typeMap}
                    positionMap={positionMap}
                    isDarkModeActive={isDarkModeActive}
                />
                <MapPointManager objects={objects} />
            </MapContainer>
        </div>
    );
};

export default MapInst;
