import React, {useCallback, useState} from "react";
import {LayersControl, TileLayer, useMap, useMapEvents} from "react-leaflet";
import {CRS} from 'leaflet';
import {useDispatch} from "react-redux";
import {setPositionCenterMapAction} from "@src/redux/reducers/app-reducer";

let timer = null;

const MapLayersControl = ({typeMap, positionMap, isDarkModeActive=false}) => {

    const
        dispatch = useDispatch();

    const
        map = useMap();

    window.__map__ = map; // TODO: :(

    const
        [nameTypeMap, setNameTypeMap] = useState(
            typeMap === 'yandex'
                ? 'Yandex'
                : typeMap === 'osm' && !isDarkModeActive
                    ? 'OpenStreetMap'
                    : 'OpenStreetMapDark'
        );

    const
        onBaseLayerChange = useCallback((event) => {
            const
                isChangePosition =
                    event.name === 'Yandex' ||
                    (event.name === 'OpenStreetMapDark' && nameTypeMap !== 'OpenStreetMap') ||
                    (event.name === 'OpenStreetMap' && nameTypeMap !== 'OpenStreetMapDark');

            isChangePosition && (map.options.crs = typeMap === 'yandex' ? CRS.EPSG3857 : CRS.EPSG3395);
            isChangePosition && map.setView(positionMap);
            setNameTypeMap(event.name);
        }, [map, nameTypeMap, positionMap, typeMap]);

    const
        onChange = useCallback(() => {
            if (timer) return;

            timer = setTimeout(() => {
                dispatch(setPositionCenterMapAction(map.getCenter()));
                clearTimeout(timer);
                timer = null;
            }, 500);
        }, [map, dispatch]);

    useMapEvents({
        baselayerchange: onBaseLayerChange,
        move: onChange
    });

    return (
        <div style={{
            display: 'none',
            overflow: 'hidden'
        }}>
            <LayersControl position="bottomleft">
                <LayersControl.BaseLayer name="Yandex" checked={typeMap === 'yandex'}>
                    <TileLayer
                        url='http://vec{s}.maps.yandex.net/tiles?l=map&v=4.55.2&z={z}&x={x}&y={y}&lang=ru_RU'
                        maxNativeZoom={17}
                        subdomains={['01', '02', '03', '04']}
                    />
                </LayersControl.BaseLayer>
                <LayersControl.BaseLayer name="OpenStreetMap" checked={typeMap === 'osm' && !isDarkModeActive}>
                    <TileLayer url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png' />
                </LayersControl.BaseLayer>
                <LayersControl.BaseLayer name="OpenStreetMapDark" checked={typeMap === 'osm' && isDarkModeActive}>
                    <TileLayer url='https://tiles.stadiamaps.com/tiles/alidade_smooth_dark/{z}/{x}/{y}{r}.png' />
                </LayersControl.BaseLayer>
            </LayersControl>
        </div>
    );
};

export default MapLayersControl;
