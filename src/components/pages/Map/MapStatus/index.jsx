import React from "react";
import {useSelector} from "react-redux";

import s from './style.module.less';
import MapStatusIndex from "./MapStatusItem";

const MapStatus = props => {

    const
        statusMap = useSelector(state => state.app.statusMap);

    return (
        <div className={s.wrapper} tabIndex={0}>
            <div className={s.hoverButton}>Cтатусы системы</div>
            <div className={s.list}>
            {
                statusMap
                    ? Object
                        .values(statusMap)
                        .map(item => <MapStatusIndex key={item['ID']} item={item} />)
                    : <span>Загрузка статусов...</span>
            }
            </div>
        </div>
    );
};

export default MapStatus;
