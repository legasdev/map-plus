import React, {useCallback} from "react";
import {useDispatch} from "react-redux";

import s from "./style.module.less";

import {openLeftPanelAction, updateFilterListAction} from "@src/redux/reducers/app-reducer";

const MapStatusIndex = ({ item }) => {

    const
        dispatch = useDispatch();

    const
        handleClickStatus = useCallback(() => {
            dispatch(openLeftPanelAction(true));
            dispatch(updateFilterListAction(item.ID === '-1' ? null : item.ID));
        }, [dispatch, item]);

    return (
        <div className={s.item} onClick={handleClickStatus}>
            <span
                className={s.item__background}
                style={{
                    backgroundColor: item['BG_Color']
                }}
            />
            <span className={s.item__counter}>{item['Cnt']}</span>
            <span className={s.item__text}>{item['Name']}</span>
        </div>
    );
};

export default MapStatusIndex;
