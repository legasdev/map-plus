import React from "react";

import s from './style.module.less';

const MapInfoItemPhotos = props => {

    return (
        <div className={s.main}>
            <h4 className={s.title}>Фотографии</h4>
            <div className={s.wrapper}>
                <img className={s.image} src={'/img/photos/img1.jpg'} alt={''} />
                <img className={s.image} src={'/img/photos/img2.jpg'} alt={''} />
                <img className={s.image} src={'/img/photos/img3.jpg'} alt={''} />
            </div>
        </div>
    );
};

export default React.memo(MapInfoItemPhotos);
