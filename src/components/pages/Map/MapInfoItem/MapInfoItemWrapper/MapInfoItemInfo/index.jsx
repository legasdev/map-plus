import React from "react";

import s from './style.module.less';

const MapInfoItemInfo = ({info}) => {

    return (
        <div className={s.main}>
            <h4 className={s.title}>Информация</h4>
            <div className={s.wrapper}>
                {
                    (info && info?.length > 0)
                        ? info.map(item => (
                            <div className={s.info} key={item['Parameter']}>
                                <h5 className={s.info_title}>{item['Display_Name']} <span className={s.info_title__parameter}>/ {item['Parameter']}</span></h5>
                                {item['Value'] &&  <span className={s.info_value}>{item['Value']}</span>}
                            </div>
                        ))
                        : <span>Нет информации</span>
                }
            </div>
        </div>
    );
};

export default React.memo(MapInfoItemInfo);
