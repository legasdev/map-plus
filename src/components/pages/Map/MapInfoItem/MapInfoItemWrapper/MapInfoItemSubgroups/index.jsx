import React from "react";

import s from './style.module.less';

import MapInfoItemSubgroupsBlock from "./MapInfoItemSubgroupsBlock";

const MapInfoItemSubgroups = ({ info }) => {

    return (
        <div className={s.main}>
            <h4 className={s.title}>Подгруппы приборов</h4>
            {
                info && info.value && info.value.data
                    ? <MapInfoItemSubgroupsBlock
                        data={info.value}
                        name={info.name}
                    />
                    : <span className={s.errorText}>Нет информации</span>
            }
        </div>
    );
};

export default React.memo(MapInfoItemSubgroups);
