import React, {useCallback, useMemo} from "react";
import {useDispatch, useSelector} from "react-redux";
import {NavLink} from "react-router-dom";

import s from './style.module.less';

import {openLeftPanelAction, openMapInfoItem} from "@src/redux/reducers/app-reducer";

const MapInfoItemSubgroupsItem = ({item}) => {
    const
        dispatch = useDispatch();

    const
        statusMap = useSelector(state => state.app.statusMap);

    const
        group = useMemo(() =>
            item['Группа'] === 'ОДПУ'
                ? 'odpu'
                : item['Группа'] === 'КИП'
                    ? 'kip'
                    : 'ipu', [item]);

    const
        statusColor = useMemo(() => {
            return statusMap ? Object.values(statusMap).find(status =>
                status['Name'] === item['Статус'])?.BG_Color : '#ffffff';
        }, [statusMap, item]);

    const
        handleClick = useCallback(() => {
            dispatch(openMapInfoItem());
            dispatch(openLeftPanelAction(false));
        }, [dispatch]);

    return (
        <NavLink onClick={handleClick} to={`/${group}/${item['_ID_Parent']}/${item['_ID_Object']}`} className={s.main}>
            <div className={s.status} style={{background: statusColor}} />
            <span className={s.name}>{item['Наименование']}</span>
            <span className={s.info}>Ресурс: {item['Ресурс']}</span>
            <span className={s.info}>Тип: {item['Тип']}</span>
            <span className={s.statusText}>{item['Статус']}</span>
        </NavLink>
    );
};

export default React.memo(MapInfoItemSubgroupsItem);
