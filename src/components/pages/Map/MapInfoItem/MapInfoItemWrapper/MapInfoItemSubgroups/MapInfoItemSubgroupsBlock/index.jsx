import React from "react";
import {useSelector} from "react-redux";

import s from './style.module.less';

import MapInfoItemSubgroupsItem from "../MapInfoItemSubgroupsItem";

const MapInfoItemSubgroupsBlock = ({data: {data, badCount, warnCount}, name}) => {

    const
        statusMap = useSelector(state => state.app.statusMap);

    return (
        <div className={s.main}>
            <header className={s.header}>
                <h5 className={s.title}>{name}</h5>
                <div><span>Датчиков: </span>
                    {badCount > 0 && <><span style={{color: statusMap[3]?.BG_Color}}>{badCount}</span>/</>}
                    {warnCount > 0 && <><span style={{color: statusMap[1]?.BG_Color}}>{warnCount}</span>/</>}
                    <span style={{color: statusMap[0]?.BG_Color}}>{data?.length - badCount - warnCount}</span>/{data?.length}
                </div>
            </header>
            <div className={s.wrapper}>
                {
                    (data && data.length > 0)
                        ? data.map(item => <MapInfoItemSubgroupsItem key={item['_ID_Object']} item={item} />)
                        : <span>Нет информации</span>
                }
            </div>
        </div>
    );
};

export default React.memo(MapInfoItemSubgroupsBlock);
