import React from "react";

import s from './style.module.less';
import MapInfoItemBaseObject from "./MapInfoItemBaseObject";

const MapInfoItemBaseStation = ({odpu, kip, ipu, object}) => {

    return (
        <div className={s.main}>
            <h4 className={s.title}>Базовые станции</h4>
            <div className={s.wrapper}>
                {
                    odpu.data &&
                    <MapInfoItemBaseObject
                        info={odpu}
                        type={'odpu'}
                        name={'ОДПУ'}
                        idObject={object._ID_Object}
                    />
                }
                {
                    ipu.data &&
                    <MapInfoItemBaseObject
                        info={ipu}
                        type={'ipu'}
                        name={'ИПУ'}
                        idObject={object._ID_Object}
                    />
                }
                {
                    kip.data &&
                    <MapInfoItemBaseObject
                        info={kip}
                        type={'kip'}
                        name={'КИП'}
                        idObject={object._ID_Object}
                    />
                }
                {
                    (!!!kip?.data && !!!ipu?.data && !!!odpu?.data) &&
                    <span>Нет информации</span>
                }
            </div>
        </div>
    );
};

export default React.memo(MapInfoItemBaseStation);
