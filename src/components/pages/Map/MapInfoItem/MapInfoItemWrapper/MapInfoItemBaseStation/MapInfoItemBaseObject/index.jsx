import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {NavLink} from "react-router-dom";

import s from "./style.module.less";

import {openMapInfoItem, openLeftPanelAction} from "@src/redux/reducers/app-reducer";

const MapInfoItemBaseObject = ({info: {data, badCount, warnCount}, type='', name='', idObject}) => {

    const
        $d = useDispatch();

    const
        statusMap = useSelector(state => state.app.statusMap);

    const
        onLinkClick = () => {
            $d(openMapInfoItem());
            $d(openLeftPanelAction(false));
        };

    return (
        <NavLink to={`/${type}/${idObject}`} className={s.item} onClick={onLinkClick}>
            <div className={s.item__status} style={{
                backgroundColor: parseFloat(badCount) !== 0
                    ? statusMap[3]?.BG_Color
                    : parseFloat(warnCount) !== 0
                        ? statusMap[1]?.BG_Color
                        : statusMap[0]?.BG_Color
            }} />
            <img
                className={s.item__icon}
                src={type === 'odpu'
                    ? '/img/icons/ic_odpu.svg'
                    : type === 'ipu'
                        ? '/img/icons/ic_ipu.svg'
                        : '/img/icons/ic_kip.svg'}
                alt={''}
            />
            <span className={s.item__name}>{name}</span>
            <div className={s.item__info}>
                {badCount > 0 && <><span style={{color: statusMap[3]?.BG_Color}}>{badCount}</span>/</>}
                {warnCount > 0 && <><span style={{color: statusMap[1]?.BG_Color}}>{warnCount}</span>/</>}
                <span style={{color: statusMap[0]?.BG_Color}}>{data?.length - badCount - warnCount}</span>/{data?.length}
            </div>
        </NavLink>
    );
};

export default React.memo(MapInfoItemBaseObject);
