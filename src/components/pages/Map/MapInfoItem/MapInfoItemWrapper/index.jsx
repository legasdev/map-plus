import React, {useMemo, useState} from "react";
import {useSelector} from "react-redux";

import s from './style.module.less';

import MapInfoItemHeader from "./MapInfoItemHeader";
import MapInfoItemBaseStation from "./MapInfoItemBaseStation";
import MapInfoItemPhotos from "./MapInfoItemPhotos";
import MapInfoItemInfo from "./MapInfoItemInfo";
import MapInfoItemSubgroups from "./MapInfoItemSubgroups";

const MapInfoItemWrapper = props => {

    const
        data = useSelector(state => state.app.mapInfoItem),
        statusMap = useSelector(state => state.app.statusMap),
        objects = useSelector(state => state.objects.objects);

    const
        [selectedGroup, setSelectedGroup] = useState(data?.odpu && 'ОДПУ');

    const
        object = useMemo(() => {
            try {
                return getObjectById(
                    objects,
                    data?.objectInfo?.IDObjectNodeType,
                    data?.objectInfo?.id
                );
            } catch(error) {
                console.group('=== Ошибка определения объекта ===');
                console.error(error);
                console.groupEnd();
            }
        }, [objects, data]);

    const
        subgroupsData = useMemo(() => {
            if (!data) {
                return;
            }

            switch(selectedGroup) {

                case 'ОДПУ':
                {
                    return {
                        value: data?.odpu,
                        name: 'ОДПУ'
                    };
                }

                case 'ИПУ':
                {
                    return {
                        value: data?.ipu,
                        name: 'ИПУ'
                    };
                }

                case 'КИП':
                {
                    return {
                        value: data?.kip,
                        name: 'КИП'
                    };
                }

                default: return;
            }
        }, [data, selectedGroup]);

    return (
        <div className={s.main}>
            {
                object
                    ? <>
                        <MapInfoItemHeader
                            status={statusMap[object['_ID_Object_Status']]}
                            object={object}
                        />
                        <div className={s.nav}>
                            {
                                data && data.odpu?.data && <button
                                    className={`${s.buttonTab} ${selectedGroup === 'ОДПУ' ? s.buttonTab__active : ''}`}
                                    onClick={() => setSelectedGroup('ОДПУ')}
                                >ОДПУ</button>
                            }
                            {
                                data && data.ipu?.data && <button
                                    className={`${s.buttonTab} ${selectedGroup === 'ИПУ' ? s.buttonTab__active : ''}`}
                                    onClick={() => setSelectedGroup('ИПУ')}
                                >ИПУ</button>
                            }
                            {
                                data && data.kip?.data && <button
                                    className={`${s.buttonTab} ${selectedGroup === 'КИП' ? s.buttonTab__active : ''}`}
                                    onClick={() => setSelectedGroup('КИП')}
                                >КИП</button>
                            }
                        </div>
                        {
                            subgroupsData &&
                            <MapInfoItemSubgroups info={subgroupsData} />
                        }
                        <MapInfoItemInfo info={data?.data?.row} />
                        {/*<MapInfoItemBaseStation*/}
                        {/*    odpu={data?.odpu}*/}
                        {/*    kip={data?.kip}*/}
                        {/*    ipu={data?.ipu}*/}
                        {/*    object={object}*/}
                        {/*/>*/}
                    </>
                    : <span>Не удалось загрузить информацию</span>
            }
        </div>
    );
};

function getObjectById(objects, numberTypeObject, idObject) {
    return objects[numberTypeObject]
        ?.objects
        ?.filter(item => parseInt(item['_ID_Object']) === parseInt(idObject))
        ?.shift();
}

export default React.memo(MapInfoItemWrapper);
