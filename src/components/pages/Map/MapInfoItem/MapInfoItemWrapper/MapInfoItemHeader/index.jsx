import React from "react";

import s from './style.module.less';

const MapInfoItemHeader = ({status, object}) => {

    return (
        <>
            <div className={s.main} style={{
                backgroundColor: status['BG_Color']
            }}>{status['Name']}</div>
            <div className={s.wrapper}>
                <div className={s.info}>
                    <h2>{object['Наименование']}</h2>
                    <span className={s.type}>{object['Тип']}</span>
                    <span className={s.address}>{object['Адрес']}</span>
                </div>
            </div>
        </>
    );
};

export default React.memo(MapInfoItemHeader);
