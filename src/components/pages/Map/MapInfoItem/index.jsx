import React, {useCallback} from "react";
import {useDispatch, useSelector} from "react-redux";

import s from './style.module.less';

import {openMapInfoItem} from "@src/redux/reducers/app-reducer";
import Popup from "@src/components/common/Popup";
import MapInfoItemWrapper from "./MapInfoItemWrapper";

const MapInfoItem = props => {

    const
        $d = useDispatch();

    const
        isMapInfoItemOpen = useSelector(state => state.app.status.isMapInfoItemOpen),
        isInfoFetching = useSelector(state => state.app.status.fetch.isLoadedInfoByObject);

    const
        onClose = useCallback(() => {
            $d(openMapInfoItem());
        }, [$d]);

    return isMapInfoItemOpen && <Popup onWrapperClose={onClose}>
            <div className={s.main}>
                {
                    isInfoFetching
                        ? <p>Загрузка информации...</p>
                        : <MapInfoItemWrapper />
                }
            </div>
        </Popup>
};

export default React.memo(MapInfoItem);
