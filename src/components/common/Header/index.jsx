import React from "react";
import {NavLink} from "react-router-dom";

import s from './style.module.less';

const Header = ({name='Нет названия', counterName='', type='', address='', link}) => {

    return (
        <div className={s.main}>
            {
                counterName && <>
                    <span className={s.name}>{counterName}</span>
                    <TextSeparator />
                </>
            }
            <span className={s.name}>{name}</span>
            <TextSeparator />
            <span className={s.text}>{type}</span>
            <TextSeparator />
            <span className={s.text}>{address}</span>
            {
                counterName && <>
                    <TextSeparator />
                    <NavLink className={s.link} to={`/${link.group}/${link.objectId}/`}>К счетчикам</NavLink>
                </>
            }
        </div>
    );
};

const TextSeparator = () => <span className={s.separator}>|</span>;


export default Header;
