import React from "react";
import {useDispatch, useSelector} from "react-redux";

import s from './style.module.less';
import {changeResourceAction, COUNTER_STATUS} from "@src/redux/reducers/counters-reducer";

const ResourceNavigation = props => {

    const
        dispatch = useDispatch();

    const
        resourceList = useSelector(state => state.counters.resourceList),
        activeResource = useSelector(state => state.counters.status[COUNTER_STATUS.resource]);

    const
        handleClick = (event) => {
            const
                value = event.target.dataset.id;

            if (value === activeResource) {
                dispatch(changeResourceAction(COUNTER_STATUS.resource, null));
            } else {
                dispatch(changeResourceAction(COUNTER_STATUS.resource, event.target.dataset.id));
            }
        };

    return (
        <div className={s.main}>
            {
                resourceList &&
                (Array.isArray(resourceList) ? resourceList : [resourceList]).map(resource => (<button
                    key={resource['ID_Object_Resource']}
                    data-id={resource['ID_Object_Resource']}
                    className={`${s.button} ${activeResource === resource['ID_Object_Resource'] ? s.button__active : ''}`}
                    onClick={handleClick}
                >
                    {resource['Object_Resource_Name']}
                </button>))
            }
        </div>
    );
};

export default ResourceNavigation;
