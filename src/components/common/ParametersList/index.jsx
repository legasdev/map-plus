import React, {useCallback} from "react";
import {useDispatch} from "react-redux";

import s from "./ParametersList.module.less";

import {changeResourceAction, COUNTER_STATUS} from "@src/redux/reducers/counters-reducer";

const ParametersList = ({ counterParams=[], selectedParams, handleChangeParameters }) => {

    console.log('selectedParams', selectedParams)
    console.log('counterParams', counterParams)

    const
        dispatch = useDispatch();

    const
        handleCloseClick = () => {
            dispatch(changeResourceAction(COUNTER_STATUS.showParameters, false));
        };

    const
        handleChange = useCallback((parameter) => () => handleChangeParameters(parameter), [handleChangeParameters]);

    return <div className={s.main}>
        <header className={s.header}>
            <h3>Параметры</h3>
            <button onClick={handleCloseClick} className={s.close_button}><i/><i/></button>
        </header>
        <div className={s.wrapper}>
            <div className={s.overflow}>
            {
                counterParams?.length
                    ? counterParams.map(param => (<label className={s.input_block} key={param['Parameter']}>
                        <input
                            type="checkbox"
                            onChange={handleChange(param['Parameter'])}
                            checked={selectedParams.find(selectParam => selectParam === param['Parameter'])}
                        />
                        <span className={s.label}>{param['Display_Name']}</span>
                    </label>))
                    : <span>Параметров нет</span>
            }
            </div>
        </div>
    </div>;
};

export default ParametersList;