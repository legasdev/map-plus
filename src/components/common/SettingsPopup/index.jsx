import React, {useCallback} from "react";
import {useDispatch, useSelector} from "react-redux";

import s from "./style.module.less";

import {changeDarkStyle, openSettingsAction, setTypeMap} from "@src/redux/reducers/app-reducer";
import Popup from "./../Popup";
import CheckBox from "./../CheckBox";

const SettingsPopup = ({onWrapperClose}) => {

    const
        $d = useDispatch();

    const
        isSettingsOpen = useSelector(state => state.app.status.isSettingsOpen),
        isDarkModeActive = useSelector(state => state.app.status.isDarkStyle),
        typeMap = useSelector(state => state.app.status.typeMap);

    const
        onOpen = useCallback(() => {
            $d(openSettingsAction(!isSettingsOpen));
        }, [$d, isSettingsOpen]),

        onChangeDarkMode = useCallback(() => {
            $d(changeDarkStyle(!isDarkModeActive, typeMap));
        }, [$d, isDarkModeActive, typeMap]),

        onChangeTypeMap = useCallback((status) => {
            if (status) {
                $d(setTypeMap('yandex'));
                $d(changeDarkStyle(isDarkModeActive, 'yandex'));
            } else {
                $d(setTypeMap('osm'));
                $d(changeDarkStyle(isDarkModeActive, 'osm'));
            }
        }, [$d, isDarkModeActive]);

    return isSettingsOpen && <Popup onWrapperClose={onOpen}>
        <div className={s.main}>
            <h3>Настройки</h3>
            <div className={s.wrapper}>
                <div className={s.wrapper_item}>
                    <CheckBox
                        id={'settings_dark_theme'}
                        handlerClick={onChangeDarkMode}
                        labels={{
                            on: 'Light',
                            off: 'Dark'
                        }}
                        checked={isDarkModeActive}
                    />
                </div>
                <div className={s.wrapper_item}>
                    <CheckBox
                        id={'settings_type_map'}
                        handlerClick={onChangeTypeMap}
                        labels={{
                            on: 'OSM',
                            off: 'Yandex'
                        }}
                        checked={typeMap === 'yandex'}
                    />
                </div>
            </div>
        </div>
    </Popup>
};

export default SettingsPopup;
