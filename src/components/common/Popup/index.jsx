import React from "react";
import ReactDOM from "react-dom";

import s from './style.module.less';

const Popup = ({onWrapperClose, isShowCloseButton=true, style={}, ...props}) => {

    return ReactDOM.createPortal(
        <div className={s.main}>
            <div className={s.shadow} onClick={onWrapperClose}/>
            {
                isShowCloseButton &&
                <div className={s.cross} onClick={onWrapperClose}><i/><i/></div>
            }
            {
                props.children
            }
        </div>,
        document.querySelector('#portals')
    );
};

export default Popup;
