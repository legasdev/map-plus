import React, {useCallback} from "react";
import {useDispatch, useSelector} from "react-redux";

import s from './style.module.less';

import {logout} from "@src/redux/reducers/auth-reducer";

const ButtonLogout = props => {

    const
        $d = useDispatch();

    const
        isDarkStyle = useSelector(state => state.app.status.isDarkStyle);

    const
        onClickLogout = useCallback(() => {
            $d(logout());
        }, [$d]);

    return (
        <button
            className={s.wrapper}
            onClick={onClickLogout}
            tabIndex={0}
        >
            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                <g id="log-out" transform="translate(1 1)">
                    <path id="Path_9" data-name="Path 9" d="M9,21H5a2,2,0,0,1-2-2V5A2,2,0,0,1,5,3H9" transform="translate(-3 -3)" fill="none" stroke={isDarkStyle ? '#E32213' : '#67120c'} strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"/>
                    <path id="Path_10" data-name="Path 10" d="M16,17l5-5L16,7" transform="translate(-3 -3)" fill="none" stroke={isDarkStyle ? '#E32213' : '#67120c'} strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"/>
                    <line id="Line_6" data-name="Line 6" x1="12" transform="translate(6 9)" fill="none" stroke={isDarkStyle ? '#E32213' : '#67120c'} strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"/>
                </g>
            </svg>
        </button>
    );
};

export default ButtonLogout;
