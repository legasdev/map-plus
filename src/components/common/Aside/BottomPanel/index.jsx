import React from "react";

import s from "./style.module.less";

import ButtonSettings from "./ButtonSettings";
import ButtonLogout from "./ButtonLogout";

const BottomPanel = props => {

    return (
        <div className={s.wrapper}>
            {/*<ButtonSettings />*/}
            <ButtonLogout />
        </div>
    );
};

export default BottomPanel;
