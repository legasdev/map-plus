import React, {useCallback} from "react";
import {useDispatch, useSelector} from "react-redux";

import s from "./style.module.less";

import {openLeftPanelAction} from "@src/redux/reducers/app-reducer";

const ButtonOpenLeftPanel = props => {

    const
        $d = useDispatch();

    const
        isLeftPanelOpen = useSelector(state => state.app.status.isLeftPanelOpen);

    const
        onClickOpenLeftPanel = useCallback(() => {
            $d(openLeftPanelAction(!isLeftPanelOpen));
        }, [$d, isLeftPanelOpen]);
    return (
        <button
            className={`${s.button} ${isLeftPanelOpen ? s.button__open : ''}`}
            onClick={onClickOpenLeftPanel}
            tabIndex={0}
        >
            <span className={`${s.button_item} ${s.button_item__first}`} />
            <span className={`${s.button_item} ${s.button_item__center}`} />
            <span className={`${s.button_item} ${s.button_item__last}`} />
        </button>
    );
};

export default ButtonOpenLeftPanel;
