import React, {useEffect, useMemo, useState} from "react";
import { NavLink, useRouteMatch, useParams } from "react-router-dom";

import s from './style.module.less';
import {appAPI} from "@src/api/api";
import toXmlRequest from "@src/utils/toXmlRequest";
import xml2object from "@src/utils/xml2object";

const MainLinksNavigate = props => {

    const
        { url } = useRouteMatch(),
        urlParams = useParams();

    const
        [isFetched, setIsFetched] = useState(false),
        [groups, updateGroups] = useState([]);

    const
        objectId = useMemo(() => urlParams.objectId, [urlParams]);

    useEffect(() => {
        if (url?.includes('map') || isFetched) {
            return;
        }

        (async () => {
            const response = await appAPI.getData(toXmlRequest({
                operation: 'GEO_Get_Object_Group_List',
                parameters: [
                    {id_object: objectId},
                    {recursive: '1'}
                ]
            }));

            const {xml_doc: {response_data: {row: data}}} = xml2object(response.data);

            if ( !data ) {
                updateGroups(null);
                return;
            }

            const arrayData = Array.isArray(data) ? data : [data];

            updateGroups(arrayData?.map(group => Object.values(group)).flat());
        })();

        setIsFetched(true);

    }, [url, isFetched, objectId]);

    return (
        <div className={s.main}>
            <NavLink to={'/map'} className={s.link} activeClassName={s.link__active}>
                <svg xmlns="http://www.w3.org/2000/svg" width="23" height="21" viewBox="0 0 23 21">
                    <g transform="translate(0.5 0.5)">
                        <path d="M1,6V22l7-4,8,4,7-4V2L16,6,8,2Z" transform="translate(-1 -2)" fill="#fff" stroke="#144468" strokeLinecap="round" strokeLinejoin="round" strokeWidth="1"/>
                        <line y2="16" transform="translate(7)" strokeWidth="1" stroke="#144468" strokeLinecap="round" strokeLinejoin="round" fill="none"/>
                        <line y2="16" transform="translate(15 4)" strokeWidth="1" stroke="#144468" strokeLinecap="round" strokeLinejoin="round" fill="none"/>
                    </g>
                </svg>
                <span className={s.link_title}>Карта</span>
            </NavLink >
            {
                !url?.includes('map') &&
                    <>
                        {
                            groups?.includes('ОДПУ') &&
                            <NavLink to={`/odpu/${urlParams.objectId}`} className={s.link} activeClassName={s.link__active}>
                                <img className={s.link_image} src={'/img/icons/navigate/odpu.svg'} alt={''} width={40} height={30} />
                                <span className={s.link_title}>ОДПУ</span>
                            </NavLink >
                        }
                        {
                            groups?.includes('ИПУ') &&
                            <NavLink to={`/ipu/${urlParams.objectId}`} className={s.link} activeClassName={s.link__active}>
                                <img className={s.link_image} src={'/img/icons/navigate/ipu.svg'} alt={''} width={40} height={30} />
                                <span className={s.link_title}>ИПУ</span>
                            </NavLink >
                        }
                        {
                            groups?.includes('КИП') &&
                            <NavLink to={`/kip/${urlParams.objectId}`} className={s.link} activeClassName={s.link__active}>
                                <img className={s.link_image} src={'/img/icons/navigate/ic_kip.svg'} alt={''} width={40} height={30} />
                                <span className={s.link_title}>КИП</span>
                            </NavLink >
                        }
                    </>
            }
        </div>
    );
};

export default React.memo(MainLinksNavigate);
