import React from "react";

import s from "./style.module.less";

import ButtonOpenLeftPanel from "./ButtonOpenLeftPanel";
import MainLinksNavigate from "./MainLinksNavigate";

const TopPanel = props => {

    return (
        <div className={s.wrapper}>
            <ButtonOpenLeftPanel />
            <MainLinksNavigate />
        </div>
    );
};

export default TopPanel;
