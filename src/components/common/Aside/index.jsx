import React from "react";

import s from './style.module.less';

import TopPanel from "./TopPanel";
import BottomPanel from "./BottomPanel";

const AsidePanel = props => {

    return (
        <div className={s.container}>
            <TopPanel />
            <BottomPanel />
        </div>
    );
};

export default AsidePanel;
