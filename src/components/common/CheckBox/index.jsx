import React, {useCallback, useState} from "react";

import s from "./style.module.less";

const CheckBox = ({id, label, handlerClick, checked=false, labels={on: '', off: ''}}) => {

    const
        [idCheckbox] = useState(id || Math.floor(Math.random() * Date.now())),
        [isActive, setIsActive] = useState(checked);

    const
        onChange = useCallback(() => {
            setIsActive(!isActive);
            handlerClick(!isActive);
        }, [isActive, handlerClick]);

    return (
        <div className={s.main}>
            <input
                type={'checkbox'}
                className={s.input}
                id={`checkbox_${idCheckbox}`}
                checked={isActive}
                onChange={onChange}
            />
            <label htmlFor={`checkbox_${idCheckbox}`} className={s.label}>
                <span>{labels.on}</span>
                <div className={s.wrapper}>
                    <div className={s.box} />
                </div>
                <span>{labels.off}</span>
            </label>
        </div>
    );
};

export default CheckBox;
