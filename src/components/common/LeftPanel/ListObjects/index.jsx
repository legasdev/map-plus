import React from "react";

import s from './style.module.less';

import ItemList from "./ItemList";
import {useSelector} from "react-redux";

const ListObjects = ({objects, isSearched=false}) => {

    const
        filterList = useSelector(state => state.app.filterList);

    return (
        objects && objects.map(({value}) => (
            ((isSearched && value.objects.length > 0) || !isSearched) &&
            <div
                key={value.name}
                className={s.wrapper}
            >
                <h2 className={s.titleGroup}>{value.name}</h2>
                {
                    value.objects.length > 0
                        ? value.objects.map(object => {
                            if ( !filterList || filterList ===  object['_ID_Object_Status']) {
                                return <ItemList
                                    key={object['_ID_Object']}
                                    id={object['_ID_Object']}
                                    name={object['Наименование']}
                                    address={object['Адрес']}
                                    type={object['Тип']}
                                    objectType={object['_ID_Object_Type']}
                                    IDObjectNodeType={object['ID_Object_Node_Type']}
                                    status={object['_ID_Object_Status']}
                                    statusText={object['Статус']}
                                    position={object['_Coordinate']}
                                />
                            }

                            return false;
                        })
                        : <span className={s.defaultText}>Нет объектов.</span>
                }
            </div>
        ))
    );
};

export default ListObjects;
