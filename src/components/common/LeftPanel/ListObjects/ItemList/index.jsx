import React, {useCallback, useRef, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {useRouteMatch} from "react-router-dom";

import s from './style.module.less';

import icons from "@src/settings/icons";
import {getCoordinatePoint} from "@src/utils/getCoordinate";
import {changeChosenMarkerAction, openLeftPanelAction, openMapInfoItem} from "@src/redux/reducers/app-reducer";

const ItemList = ({id, name='Нет названия', position, address='Нет адреса', type, objectType, IDObjectNodeType, status=0, statusText=null}) => {

    const
        $d = useDispatch();

    const
        itemPositionRef = useRef();

    const
        { url } = useRouteMatch();

    const
        statusMap = useSelector(state => state.app.statusMap),
        chosenMarker = useSelector(state => state.app.chosenMarker);

    const
        [coordinate] = useState(getCoordinatePoint({'_Coordinate': position}));

    const
        onMarkerClick = useCallback(() => {
            if (coordinate[0].toLowerCase() === 'point') {

                chosenMarker && (chosenMarker.target.setIcon(icons[chosenMarker.type] || icons.default));
                chosenMarker && (chosenMarker.target.setZIndexOffset(0));
                $d(openMapInfoItem());
                $d(openLeftPanelAction(false));

                window.__map__.eachLayer((layer) => {
                    if (layer.getLatLng
                        && layer.getLatLng().lat === parseFloat(coordinate[1])
                        && layer.getLatLng().lng === parseFloat(coordinate[2]))
                    {
                        layer.setIcon(icons[`${objectType}_hover`] || icons.default);
                        layer.setZIndexOffset(200000);
                        $d(changeChosenMarkerAction({
                            target: layer,
                            latlng: layer.getLatLng(),
                            type: objectType,
                        }));
                    }
                });

                window.__map__.flyTo([coordinate[1], coordinate[2]], 18);
            }
        }, [coordinate, objectType, chosenMarker, $d]),

        onItemClick = useCallback((event) => {
            if (event.target !== itemPositionRef.current) {
                $d(openMapInfoItem(true, id, {
                    name,
                    address,
                    IDObjectNodeType,
                }));
            }
        }, [$d, id, name, address, IDObjectNodeType]);

    return (
        <div className={s.main} tabIndex={0} onClick={onItemClick}>
            <span
                className={s.statusText}
                style={(status && status !== -1) && {
                    color: statusMap ? statusMap[status]['BG_Color'] : '#ffffff'
                }}
            >{statusText ? statusText : 'Нет статуса'}</span>
            <h3 className={s.title}>{name}</h3>
            <span className={s.type}>{type}</span>
            <span className={s.address}>{address}</span>
            {
                coordinate[0].toLowerCase() === 'point' && url.includes('map') &&
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="24" viewBox="0 0 20 24"
                     ref={itemPositionRef}
                     className={s.mapPin}
                     onClick={onMarkerClick}
                >
                    <g transform="translate(-2)" pointerEvents="none">
                        <path d="M21,10c0,7-9,13-9,13S3,17,3,10a9,9,0,1,1,18,0Z" fill="none"
                              stroke="#1d86d8" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                              pointerEvents="none"
                        />
                        <circle cx="3" cy="3" r="3" transform="translate(9 7)" fill="none"
                                stroke="#1d86d8" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                pointerEvents="none"
                        />
                    </g>
                </svg>
            }
            <div
                className={s.status}
                style={status && {
                    backgroundColor: statusMap ? statusMap[status]['BG_Color'] : '#ffffff'
                }}
            />
        </div>
    );
};

export default ItemList;
