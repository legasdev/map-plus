import React, {useCallback, useState} from "react";

import s from './style.module.less';

const SearchObjects = ({searched=0, handleChange}) => {

    const
        [searchValue, setSearchValue] = useState('');

    const
        onChangeInput = useCallback((event) => {
            handleChange(event.target.value);
            setSearchValue(event.target.value);
        }, [handleChange]);

    return (
        <div className={s.main}>
            <input
                className={s.input}
                type='text'
                value={searchValue}
                onChange={onChangeInput}
                placeholder={'Поиск...'}
            />
            <div className={s.info}>
                <span>Найдено: {searched}</span>
            </div>
        </div>
    );
};

export default SearchObjects;
