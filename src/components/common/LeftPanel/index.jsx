import React, {useCallback, useEffect, useState} from "react";
import {useSelector} from "react-redux";

import s from './style.module.less';

import ListObjects from "./ListObjects";
import SearchObjects from "./SearchObjects";

function objectToArray(obj) {
    return Object.keys(obj).map(key => ({
        name: key,
        value: obj[key],
    }));
}

function filterObjects(objectsArray, searchWord='') {
    return objectsArray && objectToArray(objectsArray).map(category => {
        return {
            ...category,
            value: {
                ...category.value,
                objects: category.value.objects.filter(object => {
                    return object['Наименование'].toLowerCase().includes(searchWord.toLowerCase())
                        || object['Адрес'].toLowerCase().includes(searchWord.toLowerCase())
                })
            }
        };
    });
}

const LeftPanel = props => {

    const
        isLeftPanelOpen = useSelector(state => state.app.status.isLeftPanelOpen),
        isFetchedObjects = useSelector(state => state.objects.status.isObjectsLoaded),
        objects = useSelector(state => state.objects.objects);

    const
        [searchValue, setSearchValue] = useState(''),
        [isSearched, setIsSearched] = useState(false),
        [searchedNumber, setSearchedNumber] = useState(0),
        [searchedObjects, setSearchedObjects] = useState(filterObjects(objects, searchValue));

    const
        onChangeSearchInput = useCallback((value) => {
            setSearchValue(value);
            setSearchedObjects(filterObjects(objects, value));
            setIsSearched(value !== '');
        }, [objects]);

    useEffect(() => {
        setSearchedNumber(searchedObjects
            ? searchedObjects.reduce((num, item) => num + item.value.objects.length, 0)
            : 0
        );
    }, [searchedObjects]);

    return (
        <div className={`${s.container} ${isLeftPanelOpen ? s.container__open : ''}`}>
            <SearchObjects
                searched={searchedNumber}
                handleChange={onChangeSearchInput}
            />
            {
                isFetchedObjects
                    ? objects
                        ? searchedNumber !== 0
                            ? <ListObjects objects={searchedObjects} isSearched={isSearched} />
                            : <p className={s.listStatus}>Объекты по запросу <strong>"{searchValue}"</strong> не найдены.</p>
                        : <p>Объекты не загружены.</p>
                    : <p>Загрузка объектов...</p>
            }
        </div>
    );
};

export default LeftPanel;
